"""
This file is a guide to how to use the code
"""


import clustermodel as cl
from clustermodel import __data_dir__ 


"""
The code aims to generate a synthetic stellar cluster.
The generation is divided in the following part :
    - Generation of star masses according to a given IMF (Initial Mass Function)
    - Pairing of stras to create binary systems
    - Placing the stars in the cluster according to a given density distribution
    - Associate each star to spectra
"""

"""
Models used are explained in the 'report.pdf' file
"""

#%%
"""
Let's say we want to model a cluster that is located 8 kpc away from the Earth, with a mass of 2.44e4 Msun.
As funny as it may sound, the Arches cluster corresponds to such parameters !
So we'll start by setting up the 'cmd' object, which contains all parameters from the cluster generation
"""

# Let's import the cmd module


# The cmds module contains an object called... 'cmds'. Calling it will load default parameters
cmd = cl.cmds.cmds()

# Let's say we found in the literature that the arches cluster 
# follows an Elson, Fall & Freeman number density profile rho(R) = rho_0 * (1 + (R/a)^2)^(-(gamma+1)/2)
# Let's load that density profile in the parameters
density_parameters = {"a":0.13,
                      "gamma":2.3,
                      "rmax":3}

cmd["SPATIAL_DISTRIBUTION"] = cl.position.EFF(**density_parameters)


# Now we want to give the cluster a Kroupa IMF

cmd["IMF"] = cl.imf.kroupa()  


#%%
"""
The parameters that were entered are the default parameters.
A list of all cmd keywords and their default value is available below
Remember that the cmds object is a subclass of the 'dict' object
"""

cmd["IMF_CLASS"] = cl.imf.imf                      # Non initialized IMF base class, has the 'generate' method that defines how to use the cmd["IMF"]
cmd["IMF"] = cl.imf.kroupa()                       # Initialized IMF class



cmd["BINARY_CLASS"] = cl.binary.binary                   # Set to False instead for no binaries. Non initialized Binary base class, has the 'generate' method that manages the use of binary related entries in the cmdtionnary.
cmd["MRATIO_SEP_MODEL"] = cl.binary.model                # Non initialized class that manages BASE_MRATIOs and BASE_SEP models
cmd["BASE_MRATIO"] = cl.binary.model_mass_ratio()        # Initialized class used in 'MRATIO_SEP_MODEL', this should not be changed
cmd["BASE_SEP"] = cl.binary.model_separation()           # Initialized class used in 'MRATIO_SEP_MODEL', this should not be changed
cmd["MULTIPLICITY"] = cl.binary.model_multiplicity()     # Initialized class containing the multiplicity model


cmd["POSITION_CLASS"] = cl.position.position                                           # Non initialized class that manages the use of 'SPATIAL_DISTRIBUTION', 'MASS_SEGREGATION' and 'COORDINATES_MAKING' functions.
cmd["SPATIAL_DISTRIBUTION"] = cl.position.EFF(**{"a":0.13, "gamma":2.3, "rmax":3})     # Initialized class for the spatial distribtution. Change parameters according to the cluster
cmd["MASS_SEGREGATION"] = cl.position.mass_segregation(s = 0.3)                        # Initialized class for the mass segregation. Change parameters according to the cluster
cmd["COORDINATES_MAKING"] = cl.position.make_coordinates()                             # Initialized class that makes the coordinates of the systems. This should probably not be changed.


cmd["SPECTRAL_CLASS"] = cl.spectral.spectral                                                            # Non initialized spectral base class
cmd["READ_SPECTRA"] = cl.spectral.read_spectra                                                          # Function to read spectra
cmd["SPECTRA_FILENAME"] = __data_dir__ + "/spectra/" + "spectra_Ks.fits.gz"   # Arguments to pass to the 'READ_SPECTRA' function. Spectra must be normalized to mag = 0, with ag the filter defined in 'SPECTRA_NORMALIZATION'
cmd["SPECTRA_NORMALIZATION"] = "Ks"                                                                     # Filter to which the spectra are normalized
cmd["READ_ISOCHRONE"] = cl.spectral.load_isochrone                                                      # Function to read the isochrone
cmd["READ_ISOCHRONE_ARGS"] = {"directory": __data_dir__ + "/isochrones"}                                # Arguments to pass to the 'READ_ISOCHRONE' function
cmd["ASSOCIATE_SPECTRA"] = cl.spectral.associate_spectra                                                # Function that associates a star to a spectra
cmd["WEIGHT_SPECTRA"] = cl.spectral.weight_spectra                                                      # Function that weights the spectra for each star

cmd["EXTINCTION"] = True                                                # True/False, add extinction to the weight
cmd["EXTINCTION_FUNCTION"] = cl.spectral.add_extinction                 # Function that adds extinction to each star
cmd["TARGET_FILTER"] = "K"                                              # Filter in which to add the extinction
cmd["EXTINCTION_MAP"] = __data_dir__ +"/arches_Ks_extinction.fits"      # Extinction map to use in 'EXTINCTION_FUNCTION'
cmd["EXTINCTION_MAP_FILTER"] = "K"                                      # Filter in which the extinction map gives the extinction


cmd["BREAK_HIGH_SEPARATIONS"] = True                                    # True/False Compute the mean density in the cluster core, and break binaries that have separation higher than twice the mean distance between stars in the core
cmd["AGE_CLUSTER"] = True                                               # Compute the mass of star now versus at their birth, according to the isochrone



#%%

"""
Functions that use data files have a "READ" keyword

For example the keyword SPECTRA_FILENAME defines the path to the spectra file, and
the keyword READ_SPECTRA defines the function used to read this file.

Another example is the READ_ISOCHRONE that defines how to read the isochrone. For this 
one, since the default isochrone is made of 2 parts of 2 isochrones, there is a single 
isochrone file, but rather the keyword READ_ISOCHRONE_ARGS, that composes all the arguments
required for the READ_ISOCHRONE function to work.

"""

#%% 

"""
Let's generate our cluster.
At this point, we have the 'cmd' variable that contains all simulation parameters.
"""

# We can initiate our cluster
mass = 2.44e4   #msun
distance = 8000 #pc

thecluster = cl.cluster.cluster(total_mass = mass, distance = distance, cmds = cmd)

#The cluster is initiated, let's generate it

thecluster.generate()


# The cluster has now been generated, we can save it now.
# Note that it is quite heavy because the default spectra are extremely well sampled
thecluster.write("cluster.fits")

#%%
# Be careful, the cmds object will not be saved with the cluster object.
# It needs to be saved in its own file.
cmd.save("cmd.cmds")

# Let's reload it
del cmd
cmd = cl.cmds.cmds("cmd.cmds")


#%%

"""

The cluster object has 3 main attributes : self.data, self.isochrone, self.spectraldata


--------
Notes on the self.data Table
--------
self.data is an Astropy Table that contains all the informations about the cluster.

- 'Mass_ini' : Mass of the star at its formation
- 'Mass_[Msun]' : Current mass of the star
- 'x_[pc]' : x coordinate of the star in the xyz system, with point (0,0,0) at the center of the cluster
- 'y_[pc]' : x coordinate of the star in the xyz system, with point (0,0,0) at the center of the cluster
- 'y_[pc]' : x coordinate of the star in the xyz system, with point (0,0,0) at the center of the cluster
- 'xproj_[arcsec]' : Sky projection coordinate, point (0,0) the center of the cluster
- 'yproj_[arcsec]' : Sky projection coordinate, point (0,0) the center of the cluster
- 'Multiplicity' : number of star in the system
- 'Linked_to' : if the system is a binary system, position in the list of the paired star (usually the next or the previous one). If not paired, linked to itself
- 'ref' : reference of the spectrum that the star is associated with
- 'weight' : weight to apply to the spectrum for that star
- 'weight_ext_FILTERNAME' : weight to apply to the spectrum to get the flux including extinction in the filter FILTERNAME

--------
Notes on the self.isochrone Table
--------
self.isochrone is an Astropy Table that contains all the informations about the isochrone that was used in the generation of the cluster

- 'Mass_ini' : Initial mass
- 'Mass' : Current mass
- 'Teff' : Effective temperature
- 'U' : U absolute magnitude - Bessel filter
- 'B' : B absolute magnitude - Bessel filter
- 'V' : V absolute magnitude - Bessel filter
- 'R' : R absolute magnitude - Bessel filter
- 'I' : I absolute magnitude - Bessel filter
- 'J' : J absolute magnitude - 2MASS filter
- 'H' : H absolute magnitude - 2MASS filter
- 'Ks' : Ks absolute magnitude - 2MASS filter

--------
Notes on the self.spectraldata FITS_REC array
--------
self.spectraldata contains the various spectra used for this cluster

- 'Teff (K)' : Effective temperature of the spectra
- 'Lambda (Angstrom)' : Contains a list of wavelengths for each spectrum
- 'Flux (erg/cm2/s/A)' : Contains the values for each spectrum

"""

#%%




