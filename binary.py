#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 14:13:37 2020

@author: demarsd
"""


import numpy as np
from scipy.interpolate import interp1d
from os.path import join
from . import utils as ut

stdatadir = join(ut.__pkg_dir__, "data")

import astropy.units as u
import astropy.constants as const



class log_normal:
    """
    log normal class, used in the separation distributions.
    """
    def __init__(self, log_mean, sigma):
        self.mean= log_mean
        self.sigma = sigma
        self.make_distrib()
    
    def evaluate(self, x):
        val =  (1/(self.sigma * np.sqrt(2*np.pi))) * np.exp(-(np.log10(x)-self.mean)**2 / (2 * self.sigma**2))
        return val
    
    def make_distrib(self, low = None, up = None, size = 3, length = 2001):
        if low is None:
            low = self.mean - size*self.sigma
        if up is None:
            up = self.mean + size * self.sigma
        
        oldx = np.linspace(low,up, length, endpoint = True)
        x = [10**(oldx[i]) for i in range(len(oldx))]
        y = [self.evaluate(x[i]) for i in range(len(x))]
        integ = [np.trapz(y[0:k+1], oldx[0:k+1]) for k in range(len(y))]
        norm = integ[-1]
        integ = np.dot(integ, 1/norm)
        fun = interp1d(integ, oldx)
        self.function = fun
    
    def random(self):
        c = np.random.uniform()
        return 10**self.function(c)





class binary:
    """
    Binary class, called in cluster object 'generate' method
    Manages the pairing of binary systems
    """
    def __init__(self, data = None, cmds = None, **kwargs):
        print("Initializing binary object functions")
        if cmds is None:
            from .cluster import load_default_parameters
            cmds = load_default_parameters()
        for key in kwargs:
            if key in cmds:
                cmds[key] = kwargs[key]
        self.cmds = cmds
        if data is not None:
            self.data = data
    
    
    def generate(self, data = None, masses = None):
        """
        Make paired systems. Computing pairing probability, mass_ratio and separation.
        Returns : systems = [mass1, mass2, separation(au)]
        """
        
        # Sorting stars by decreasing mass
        print("Sorting stars by decreasing mass, this may take some time")
        if masses is not None:
            masses = np.sort(masses)[::-1]
            masses_ini = np.copy(masses)
        else:
            if data is not None:
                masses = data["Mass_[Msun]"]
                masses_ini = data["Mass_ini"]
            else:
                masses = self.data["Mass_[Msun]"]
                masses_ini= self.data["Mass_ini"]
            
            lenmax = len(masses)
            finmass = []
            inimass = []
            for i in range(lenmax):
                maxnow = np.max(masses)
                idx,val = ut.find_nearest(masses,maxnow)
                finmass += [masses[idx]]
                inimass += [masses_ini[idx]]
                masses = np.delete(masses, idx)
                masses_ini= np.delete(masses_ini, idx)
            masses = np.copy(finmass)
            masses_ini = np.copy(inimass)
        
        print("Computing mass_ratios and separation functions")
        binarfun = self.cmds["MRATIO_SEP_MODEL"](self.cmds)  #mass_ratio and separation function
        # Checking if 
        multifun = self.cmds["MULTIPLICITY"]
        print("Computing pairing probability, separations, mass_ratios, and paired systems")
        final_systems = []
        number = 0
        n=0
        lentot = len(masses_ini)
        while len(masses_ini) >= 2:
            n+=1
            mass = masses[0]
            
            # Pairing probability according to Lu+2013 Multiplicity Fraction
            # Note that there is the assumption that Multiplicity Fraction == Binary Fraction
            # There are no higher order systems
            prob = multifun.random(mass)
            randomize = np.random.uniform(0.0, 1.0)
            
            # If randomize > prob, no pairing, single-star system
            if randomize > prob:
                to_delete = [0]
                # separation = None
                system = [masses_ini[0], None]
            # If randomize <= prob, pairing, binary system
            else:
                n+=1
                number +=1
                # separation method
                mratio,separation = binarfun.random(mass)
                mass1 = masses_ini[0]
                
                masses = np.delete(masses, [0])
                masses_ini = np.delete(masses_ini, [0])
                paired_idx, paired_mass = ut.find_nearest(masses, mass*mratio)
                
                
                to_delete = [paired_idx]
            
                #Make system
                system = [mass1, masses_ini[paired_idx], separation]
            
            
            
            # Add system to list of systems
            final_systems.append(system)
            
            # Remove paired star from list of stars
            masses = np.delete(masses, to_delete)
            masses_ini = np.delete(masses_ini, to_delete)
            
            
            ut.printProgressBar(n,lentot)
        # If there is a remaining single star, add it to final_systems as a single-star system
        if len(masses_ini) == 1:
            n+=1
            final_systems.append([masses_ini[0], None])
            ut.printProgressBar(n,lentot)
        
        
        print("Binary systems : {}".format(number))
        # Returns the systems with the masses as the initial mass of the stars        
        return final_systems
    


class model_multiplicity:
    """
    Multiplicity model, uses Lu+2013
    """
    def __init__(self):
        """
        Pairing probability accord to Lu+2013 Fig16
        MF(mass) = A * m**gamma, A=0.44, gamma = 0.51
        """
        self.A = 0.44
        self.gamma = 0.51
    def random(self, mass):
        prob = self.A * mass**self.gamma
        if prob > 1.0:
            prob = 1.0
        return prob




class mass_ratio_and_separation:
    """
    Base model, called in the 'model' class
    """
    def __init__(self, cmds):
        self.massratio = cmds["BASE_MRATIO"]
        self.separation = cmds["BASE_SEP"]
    
    def random(self, mass, sep_lim = 10000):
        while True:
            sep = self.separation.random(mass)
            mratio = self.massratio.random(mass, sep = sep)
            if mratio is not ('outsep' or 'outmass'):
                break
        return mratio, sep
        


class model:
    """
    The class model that defines how mass ratios and separations are computed
    """
    def __init__(self, cmds):
        self.elbadry = el_badry()
        self.basemodel = mass_ratio_and_separation(cmds)
        self.rag = raghavan()
    
    def random(self, mass, sep_min = 1, sep_max = 10000):
        """
        Generates the mass ratio and separation depending on the primary mass
        sep_min and sep_max define the limits for the separation
        """
        mass_in_kg = ((mass * u.M_sun).to(u.kg)).value
        sep_flag = False
        while sep_flag == False:
            
            if mass < 0.7:
                # Basic case where everything is well, first the separation is generated, then the mass_ratio.
                mratio, sep = self.basemodel.random(mass)
            
            elif mass >= 2.5:
                # Generating period according to raghavan
                period = self.rag.random() #converting from days to seconds
                period = period * 24 * 3600
                
                # phase on the orbit
                t_orbit = np.random.uniform(0, period)
                
                # mean movement
                n = 2*np.pi / period
                
                # excentricity
                exc = np.random.uniform(0.05,0.9)
                
                # excentric anomaly
                uf = np.linspace(0,2*np.pi,1000)
                
                t = 1/n * (uf - exc * np.sin(uf))
                
                # numerically converting time into excentric anomaly 
                fun = interp1d(t, uf)
                
                # the excentric anomaly on this orbit
                uc = fun(t_orbit)
                
                # mass_ratio
                mratio = self.basemodel.massratio.random(mass = mass)
                
                # rc as norm of vec(r1) - vec(r2)
                rc = (period**2 * const.G.value * mass_in_kg * (1 + mratio) / (4 * np.pi**2))**(1/3) * (1 - exc * np.cos(uc))            
                
                # separation
                sep = rc * (1/(1 + 1/mratio) + 1/(1 + mratio))
                # converting into AU
                sep = ((sep * u.m).to(u.au)).value
                
            else:
                # Here comes the Monte-Carlo-like method
                check = False
                while check == False:
                    # Generating period according to Raghavan
                    period = self.rag.random() * 24 * 3600 #converting from days to seconds
                    
                    # phase on the orbit
                    t_orbit = np.random.uniform(0, period)
                    
                    # mean movement
                    n = 2*np.pi / period
                    
                    # excentricity
                    exc = np.random.uniform(0.05,0.9)
                    
                    # Initial mratio
                    q0 = np.random.uniform(0.1,1)
                    
                    flag = False
                    mratio = q0
                    count = 0
                    while flag == False:
                        # mass_in_kg = ((mass * u.M_sun).to(u.kg)).value
                        count += 1
                        # print(count)
                        if count >=1000:
                            # print("failed")
                            break
                        # excentric anomaly
                        uf = np.linspace(0,2*np.pi,1000)
                        
                        t = 1/n * (uf - exc * np.sin(uf))
                        
                        # numerically converting time into excentric anomaly 
                        fun = interp1d(t, uf)
                        
                        # the excentric anomaly on this orbit
                        uc = fun(t_orbit)
                        
                        # rc as norm of vec(r1) - vec(r2)
                        rc = (period**2 * const.G.value * mass_in_kg / (4 * np.pi**2))**(1/3) * (1 - exc * np.cos(uc))            
                        
                        # separation
                        sep = rc * (1/(1 + 1/mratio) + 1/(1 + mratio))
                        # converting into AU
                        sep = ((sep * u.m).to(u.au)).value
                        
                        # Computing a new mass_ratio with elbadry and this last separation value
                        newmratio = self.elbadry.random(mass, sep)
                        if newmratio == 'outsep' or newmratio == 'outsep':
                            # print("failed badry", sep, newmratio)
                            break
                        
                        # Computing separation with this new mass_ratio
                        rc = (period**2 * const.G.value * mass_in_kg * (1 + newmratio) / (4 * np.pi**2))**(1/3) * (1 - exc * np.cos(uc))            
                        newsep = rc * (1/(1 + 1/newmratio) + 1/(1 + newmratio))
                        newsep = ((newsep * u.m).to(u.au)).value
                        
                        # Checking that this new separation is in the same bin as the previous
                        sepmin = self.elbadry.sep_min
                        sepmax = self.elbadry.sep_max
                        # Searching for separation bin
                        otherflag1 = False
                        for i in range(len(sepmin)):
                            if sep >= sepmin[i] and sep<= sepmax[i]:
                                sep_idx = i
                                otherflag1 = True
                                break
                        otherflag2 = False
                        for i in range(len(sepmin)):
                            if newsep >= sepmin[i] and newsep<= sepmax[i]:
                                newsep_idx = i
                                otherflag2 = True
                                break
                            
                        # If separation is out of El-Badry range
                        # Then go to next iteration and compute from newmratio
                        if (otherflag1 == False or otherflag2 == False):
                            mratio = newmratio
                            continue
                        
                        # If old and new separation aren't in the same El-Badry bin (they are not compatible)
                        # Then go to next iteration and compute from newmratio
                        elif sep_idx != newsep_idx:
                            mratio = newmratio
                            continue
                        
                        # If old and new separation are in the same El-Badry bin
                        # Then newmratio and newsep are the output ones
                        elif sep_idx == newsep_idx:
                            mratio = newmratio
                            sep = newsep
                            # print("separation", sep)
                            # print("mratio", mratio)
                            flag = True
                            check = True
                        
                        # This should never enter this else as all conditions were examined... but who knows ?
                        else:
                            raise ValueError("It shouldn't enter this 'else' ???????????")
            
            # Checking that separations are consistent with separation limits
            if sep > sep_min and sep < sep_max:
                sep_flag = True
        
        return mratio, sep





class model_mass_ratio:
    """
    The base mass ratio model 
    The code will only call that class for masses below 0.1 msun and above 2.5 msun
    """
    def __init__(self):
        self.lowduch = mass_duchene(gamma = 4.2)
        self.elbadry = el_badry()
        self.midduch = mass_duchene(gamma = -0.5)
    
    def random(self, mass, sep = None):
        if mass >= 0.1 and mass < 2.5:
            # El-Badry model
            mratio = self.elbadry.random(mass,sep)
        elif mass < 0.1:
            # Duchene model at low masses
            mratio = self.lowduch.random()
        else:
            # Duchene model at high masses
            mratio = self.midduch.random()
        
        return mratio
        




class model_separation:
    """
    separation class, Used to compute separation and mass ratio of a binary system, through the 'random' method.
    """
    def __init__(self):
        self.duch = sep_low_duchene() #Duchene dsitirbution at low masses
        self.wadu = ward_duong() # Ward-Duong distribution
    
    def random(self, mass):
        if mass >= 0.7:
            raise ValueError("Mass should be lower than 0.7 Msol in that function")
        if mass < 0.1:
            val = self.duch.random()
        elif mass >= 0.1 and mass < 0.7:
            val = self.wadu.random()
        
        # Return the separation in AU
        return val





class raghavan(log_normal):
    """
    Raghavan log-normal period distribution
    """
    def __init__(self):
        self.log_mean_period = 5.03
        self.sigma_period = 2.28
        super().__init__(self.log_mean_period, self.sigma_period)


class ward_duong(log_normal):
    """
    Ward-Duong+2015 log-normal apparent separation distribution
    """
    def __init__(self):
        self.log_mean_semiaxis = 0.77
        self.sigma_semiaxis = 1.34
        super().__init__(self.log_mean_semiaxis, self.sigma_semiaxis)
    
    def random(self):
        val = super().random() # The apparent separation value
        # Deprojecting that apparent separation with a random inclinaison
        theta = np.random.uniform(0, np.pi)
        sep = val / np.sin(theta)
        return sep

class sep_low_duchene(log_normal):
    """
    Duchene low log-normal separation distribution at low masses M < 0.1 Msun
    """
    def __init__(self):
        self.log_mean_semiaxis = np.log10(4.5)  # mean value log in semiaxis
        self.sigma_period = 0.5     #sigma log period in days
        
        # Converting the sigma that is for the period distribution, into the one for the separation distribution
        # Conversions are made with a total mass of M = 0.1 Msun.
        au = 1.5e11
        sig_trans =(np.log10(period_to_semiaxis(10**4, M = 0.1)/au) - np.log10(period_to_semiaxis(10**2, M = 0.1)/au))/(4-2)
        self.sigma_semiaxis = self.sigma_period * sig_trans
        
        super().__init__(self.log_mean_semiaxis, self.sigma_semiaxis)





def period_to_semiaxis(period, M = 1.5, G = 6.67e-11):
    """
    Convert period to semiaxis
    """
    M *= 1.98e30
    a = ( G*M * (period/(2*np.pi))**2)**(1/3)
    return a

def semiaxis_to_period(a, M = 1.5, G = 6.67e-11):
    """
    Convert semiaxis to period
    """
    M *= 1.98e30
    T = np.sqrt(a**3 * (4 * np.pi**2)/ (G*M))
    return T







class mass_duchene:
    """
    class for the duchene mass ratios.
    This distribution is a power law : q(M) = M**gamma
    """
    def __init__(self, gamma):
        self.gamma = gamma
        self.norm()
        
    def evaluate(self, q, normalize = False):
        """
        evaluate de the distribution in a given q
        """
        gamma = self.gamma
        if normalize == True:
            norm = 1
        else:
            norm = self.normfact
        val = 1/norm* q**(gamma)
        return val
    
    def random(self, vallow = 0.1):
        """
        returns a random mass ratio from the distribution
        """
        if self.gamma <0:
            c= np.random.uniform()
            gamma = self.gamma
            q = (c*(gamma+1)*self.normfact +vallow**(gamma+1))**(1/(gamma+1))
        else:
            c = np.random.uniform()
            gamma = self.gamma
            q = (c*(gamma+1)*self.normfact)**(1/(gamma+1))
        return q
    
    def norm(self, vallow = 0.1):
        """
        normalizes the distribution to 1
        """
        if self.gamma <0:
            q = np.linspace(vallow, 1, 1000)
        else:
            q = np.linspace(0,1,1000)
        vals = self.evaluate(q, normalize = True)
        integ = np.trapz(vals,q)
        self.normfact = integ



class el_badry:
    """
    mass_ratio distribution as in El-Badry2019. This class uses all parameters from their fits given in the article appendix.
    """
    def __init__(self):
        parameters = self.load_elbadry_fits()
        self.M_min = parameters[0]
        self.M_max = parameters[1]
        self.sep_min = [1,350,600,1000,2500,5000,15000]    # lowest separation edited to 1 AU (instead of 50 AU) to extend it to low separations
        self.sep_max = [350,600,1000,2500,5000,15000,50000]
        self.qbreak = [0.5,0.5,0.5,0.5,0.3]
        self.F_twin = parameters[2]
        self.q_twin = parameters[3]
        self.gamma_large = parameters[4]
        self.gamma_small = parameters[5]
        self.gamma_s = parameters[6]
        self.make_array()
    
    def norm(self):
        return 0
    
    def load_elbadry_fits(self):
        """
        Loads el-badry parameters from the file
        """
        filename = stdatadir + "/el-badry_mass_ratio_fits.txt"
        vals =np.loadtxt(filename, delimiter = ";")
        M_min = vals[0]
        M_max = vals[1]
        F_twin = vals[2:9]
        q_twin = vals[9:16]
        gamma_large = vals[16:23]
        gamma_small = vals[23:30]
        gamma_s = vals[30:37]
        return M_min, M_max, F_twin, q_twin, gamma_large, gamma_small, gamma_s
    
    def make_array(self):
        """
        Makes the el-badry distribution for all mass and separation bins.
        """
        x = np.linspace(0,1,2001, endpoint = True)
        functions = []
        subfun = []
        for i in range(len(self.sep_min)):
            sep_bin = []
            subfun_bin = []
            for j in range(len(self.M_min)):
                fun = sub_badry(self.qbreak[j], self.F_twin[i][j], self.q_twin[i][j], self.gamma_large[i][j], self.gamma_small[i][j])
                subfun_bin.append(fun)
                y = [fun.evaluate(x[k]) for k in range(len(x))]
                integ = [np.trapz(y[0:k+1], x[0:k+1]) for k in range(len(y))]
                integ = np.dot(integ, 1/integ[-1])
                newfun = interp1d(integ, x)
                sep_bin.append(newfun)
                ut.printProgressBar(i*(len(self.M_min))+j+1, len(self.sep_min)*len(self.M_min))
            functions.append(sep_bin)
            subfun.append(subfun_bin)
        self.functions = functions
        self.subfun = subfun
    
    def get_sub(self, mass, sep):
        """
        Given a mass and separations, searches for the bin in el-badry model,
        and returns the mratio dsitrbution object for that bin. See sub_badry object further in the code
        """
        # Searching for separation bin
        flag = False
        for i in range(len(self.sep_min)):
            if sep >= self.sep_min[i] and sep< self.sep_max[i]:
                sep_idx = i
                flag = True
                break
        if flag == False:
            """
            separation is out of bins ranges
            """
            # raise ValueError("given separation is out of El-Badry range, given separation : {}".format(sep))
            return 'outsep'
        
        flag = False
        # Searching for mass bin
        for i in range(len(self.M_min)):
            if mass >= self.M_min[i] and mass< self.M_max[i]:
                M_idx = i
                flag = True
                break
        if flag == False:
            """
            Mass is out of bins ranges
            """
            # warnings.warn("given mass is out of El-Badry range, given mass : {}".format(mass))
            return 'outmass'
        
        
        sub = sub_badry(self.qbreak[M_idx], self.F_twin[sep_idx][M_idx], self.q_twin[sep_idx][M_idx], self.gamma_large[sep_idx][M_idx], self.gamma_small[sep_idx][M_idx])
        
        return sub
        
    def random(self, mass, sep, q_min = 0.10):
        # Searching for separation bin
        flag = False
        for i in range(len(self.sep_min)):
            if sep >= self.sep_min[i] and sep< self.sep_max[i]:
                sep_idx = i
                flag = True
                break
        if flag == False:
            """
            separation is out of bins ranges
            """
            # raise ValueError("given separation is out of El-Badry range, given separation : {}".format(sep))
            return 'outsep'
        
        flag = False
        # Searching for mass bin
        for i in range(len(self.M_min)):
            if mass >= self.M_min[i] and mass< self.M_max[i]:
                M_idx = i
                flag = True
                break
        if flag == False:
            """
            Mass is out of bins ranges
            """
            # warnings.warn("given mass is out of El-Badry range, given mass : {}".format(mass))
            return 'outmass'
        
        func = self.functions[sep_idx][M_idx]
        
        flag = False
        while flag == False:
            mass_ratio = func(np.random.uniform())
            if mass_ratio >= q_min:
                flag = True
        return mass_ratio
        
    
class sub_badry(el_badry):
    """
    Distribution object that composes the el-badry object.
    """
    def __init__(self, qbreak, ftwin, qtwin, gamlarge, gamsmall):
        self.qbreak = qbreak
        self.ftwin = ftwin
        self.qtwin = qtwin
        self.gaml = gamlarge
        self.gams = gamsmall
        self.norm()
    
    def norm(self):
        """
        Normalizes the distribution
        """
        qbreak = self.qbreak
        ftwin = self.ftwin
        qtwin = self.qtwin
        gaml = self.gaml
        gams = self.gams
        first = 1/(gams+1) * (0.3**(gams+1) - 0.05**(gams+1))
        second = 1/(1-ftwin) * (1/(gams +1) * (qbreak**(gams+1) - 0.3**(gams+1)) + (qbreak**(gams-gaml) /(gaml+1)) * (1 - qbreak**(gaml+1)))
        self.normfact = first + second
        b = ftwin/(1-qtwin) * second
        self.b = b
    
    def evaluate(self, q, qmin = 0.10):
        """
        evaluates the distribution
        """
        qbreak = self.qbreak
        gams = self.gams
        gaml = self.gaml
        qtwin = self.qtwin
        b = self.b
        normfact = self.normfact
        
        if not hasattr(q, "__iter__"):
            qs = [q]
        else:
            qs = q
        vals = []
        for q in qs:
            if q>= 0 and q < qmin:
                vals.append(0)
            elif q >= qmin and q < qbreak:
                val = (1/normfact) * q**gams
                vals.append(val)
            elif q >= qbreak and q < qtwin:
                val = qbreak**(gams-gaml) / normfact * q**gaml
                vals.append(val)
            elif q >= qtwin and q <= 1:
                val = qbreak**(gams-gaml) / normfact * q**gaml + b/normfact
                vals.append(val)
            else:
                raise ValueError("q must be between 0 and 1")
        
        if len(vals) == 1:
            return vals[0]
        else:
            return vals
        








































