#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 14:25:12 2020

@author: demarsd
"""

# from . import utils as ut
import numpy as np
from astropy.table import Table
from astropy.io import fits
import astropy.units as u
import astropy.constants as const
import time
from scipy.interpolate import interp1d




from . import utils as ut
from . import cmds




class cluster:
    """
    Cluster class, this is the framework for clustermodel.binary, clustermodel.spectra, clustermodel.imf, clustermodel.position.
    
    Parameters
    ----------
    filename : str, optional
        File name to read the cluster data from, if it was previously saved with the 'write' method. The default is None.
    total_mass : float, optional
        Total mass of the cluster, in solar masses. Give either total_mass or number.
    number : int, optional
        number of stars in the cluster. Give either number or total_mass.
    distance : float, optional
        Distance to the cluster, in parsec
    cmds : cmds object, optional
        clustermodel.cmds.cmds object containing parameters for the generation. If not given, default parameters will be used
    **kwargs :
        cmds can be edited by entering keys and values them directly in the kwargs.


    --------
    Notes on the self.data Table
    --------
    self.data is an Astropy Table that contains all the informations about the cluster.
    
    - 'Mass_ini' : Mass of the star at its formation
    - 'Mass_[Msun]' : Current mass of the star
    - 'x_[pc]' : x coordinate of the star in the xyz system, with point (0,0,0) at the center of the cluster
    - 'y_[pc]' : x coordinate of the star in the xyz system, with point (0,0,0) at the center of the cluster
    - 'y_[pc]' : x coordinate of the star in the xyz system, with point (0,0,0) at the center of the cluster
    - 'xproj_[arcsec]' : Sky projection coordinate, point (0,0) the center of the cluster
    - 'yproj_[arcsec]' : Sky projection coordinate, point (0,0) the center of the cluster
    - 'Multiplicity' : number of star in the system
    - 'Linked_to' : if the system is a binary system, position in the list of the paired star (usually the next or the previous one). If not paired, linked to itself
    - 'ref' : reference of the spectrum that the star is associated with
    - 'weight' : weight to apply to the spectrum for that star
    - 'weight_ext_FILTERNAME' : weight to apply to the spectrum to get the flux including extinction in the filter FILTERNAME
    
    --------
    Notes on the self.isochrone Table
    --------
    self.isochrone contains all the informations about the isochrone that was used in the generation of the cluster
    
    - 'Mass_ini' : Initial mass
    - 'Mass' : Current mass
    - 'Teff' : Effective temperature
    - 'U' : U absolute magnitude - Bessel filter
    - 'B' : B absolute magnitude - Bessel filter
    - 'V' : V absolute magnitude - Bessel filter
    - 'R' : R absolute magnitude - Bessel filter
    - 'I' : I absolute magnitude - Bessel filter
    - 'J' : J absolute magnitude - 2MASS filter
    - 'H' : H absolute magnitude - 2MASS filter
    - 'Ks' : Ks absolute magnitude - 2MASS filter
    
    --------
    Notes on the self.spectraldata FITS_REC array
    --------
    self.spectraldata contains the various spectra used for this cluster
    
    - 'Teff (K)' : Effective temperature of the spectra
    - 'Lambda (Angstrom)' : Contains a list of wavelengths for each spectrum
    - 'Flux (erg/cm2/s/A)' : Contains the values for each spectrum
    """
    
    def __init__(self, filename = None, total_mass = None, number = None, distance = None, cmds = None, **kwargs):

        if filename is not None:
            dat = fits.open(filename)
            self.data = Table(dat[1].data)
            self.isochrone = Table(dat[2].data)
            self.spectraldata = dat[3].data
            
            self.total_mass = np.sum(self.data["Mass_ini"])
            self.distance = dat[0].header["DISTANCE"]
        
        else:
            self.total_mass = total_mass
            self.number = number
            self.distance = distance
            if cmds is None:
                self.cmds = load_default_parameters()
            else:
                self.cmds = cmds
                
            for key in kwargs:
                if key in cmds:
                    cmds[key] = kwargs[key]
    
    
    def generate(self, total_mass = None, number = None, distance = None):
        """
        Generation of the cluster

        Parameters
        ----------
        total_mass : float, optional
            Total mass of the cluster if it wasn't previously given in the initialization (solar masses)'
        number : int, optional
            Number of stars in the cluster if it wasn't previously given in the initialization (int).
        distance : float, optional
            Distance to the cluster if it wasn't previously given in the initialization (parsec).
        

        """
        time_ini = time.time()
        if total_mass is not None:
            self.total_mass = total_mass
        if number is not None:
            self.number = number
        if distance is not None:
            self.distance = None
        
        
        self.generate_stars_masses()
        
        self.age_cluster()
        
        self.generate_binary_systems()
        
        self.generate_stars_positions()
        
        self.z_project()
        
        self.associate_spectra()
        
        
        if self.cmds["EXTINCTION"] == True:
            self.apply_extinction(filter = self.cmds["TARGET_FILTER"])
        
        # Reordering columns
        colnames = self.data.colnames
        wname = []
        for name in colnames:
            if "weight_" in name:
                wname += [name]
                
        newdata = self.data['Mass_ini',
                              'Mass_[Msun]',
                              'x_[pc]',
                              'y_[pc]',
                              'z_[pc]',
                              'xproj_[arcsec]',
                              'yproj_[arcsec]',
                              'Multiplicity',
                              'Linked_to',
                              'ref',
                              'weight']
        for name in wname:
            newdata[name] = self.data[name]
        
        self.data = newdata
        
        
        time_end = time.time()
        elpased_time = time_end - time_ini
        minutes = elpased_time // 60
        seconds = elpased_time % 60
        
        print("Elapsed time : {}mins {:.2f}s".format(minutes, seconds))
        
    def generate_stars_masses(self):
        """
        Generate star masses according to the IMF  in self.cmds["IMF"]
        """
        
        if self.total_mass is None and self.number is None:
            raise ValueError("Please provide either 'total_mass' or 'number' when initiating the cluster." )
        
        if hasattr(self.total_mass, "__iter__"):
            masses = self.total_mass
        elif self.number is not None:
            masses = self.cmds["IMF_CLASS"](self.cmds).generate(number = self.number)
        else:
            masses = self.cmds["IMF_CLASS"](self.cmds).generate(total_mass = self.total_mass)
        
        # self.masses = masses
        self.total_mass = np.sum(masses)
        self.number = len(masses)
        
        thecluster = Table()
        thecluster["Mass_ini"] = np.sort(masses)[::-1]
        # thecluster = Table(np.sort(masses)[::-1], names = ("Mass_ini"))
        self.data = thecluster
    
    
    def age_cluster(self):
        if self.cmds["AGE_CLUSTER"] == True:
            iso = self.cmds["READ_ISOCHRONE"](**self.cmds["READ_ISOCHRONE_ARGS"])
            fun = interp1d(iso["Mass_ini"], iso["Mass"], bounds_error = False, fill_value = (iso["Mass"][0], iso["Mass"][-1]))
            self.data["Mass_[Msun]"] = fun(self.data["Mass_ini"])
        else:
            self.data["Mass_[Msun]"] = self.data["Mass_ini"]
    
    
    def generate_binary_systems(self):
        """
        Generate binary systems
        """
        if self.cmds["BINARY_CLASS"] == False:
            systems = [[m, None] for m in self.data["Mass_[Msun]"]]
        else:
            systems = self.cmds["BINARY_CLASS"](self.data, cmds = self.cmds).generate()
        
        self.systems = systems # Systems identified by the initial mass of stars
        # Age systems
        if self.cmds["AGE_CLUSTER"] == True:
            iso = self.cmds["READ_ISOCHRONE"](**self.cmds["READ_ISOCHRONE_ARGS"])
            fun = interp1d(iso["Mass_ini"], iso["Mass"], bounds_error = False, fill_value = (iso["Mass"][0], iso["Mass"][-1]))
            aged_systems = []
            for sys in systems:
                thesemasses = sys[:len(sys)-1]
                aged_sys = fun(thesemasses)
                aged_sys = np.append(aged_sys,sys[-1])
                aged_systems.append(aged_sys)
            self.aged_systems = aged_systems
        else:
            self.aged_systems = self.systems
        
    
    def generate_stars_positions(self):
        """
        Generate stars positions
        """
        thecluster = self.cmds["POSITION_CLASS"](self.cmds).generate([self.systems, self.aged_systems])
        self.data = thecluster
        self.age_cluster()
    
    
    def z_project(self):
        """
        Projects a population distribution in the (x,y) angle plane, given a distance.
        """
        if self.distance is None:
            raise ValueError("Please give the distance of the cluster with 'self.distance = value")
        x = self.data["x_[pc]"].data
        y = self.data["y_[pc]"].data
        z = self.data["z_[pc]"].data
        xproj = (np.arctan(np.divide(x,np.add(self.distance, z))) * u.rad).to(u.arcsec).value
        yproj = (np.arctan(np.divide(y,np.add(self.distance, z))) * u.rad).to(u.arcsec).value
        
        self.data["xproj_[arcsec]"] = xproj
        self.data["yproj_[arcsec]"] = yproj
        
    
    def associate_spectra(self):
        thedata, spectraldata, isochrone = self.cmds["SPECTRAL_CLASS"](self.data, cmds = self.cmds, distance = self.distance).run()
        self.spectraldata = spectraldata
        self.data = thedata
        self.isochrone = isochrone
    
    
    def get_spectra(self):
        """
        Returns spectra of the cluster
        """
        lam = self.spectraldata["Lambda (Angstrom)"]
        spec = self.spectraldata["Flux (erg/cm2/s/A)"]
        return lam, spec
    
    
    def write(self, filename, overwrite = False):
        """
        Write spectra to a file.

        Parameters
        ----------
        filename : str
            Self explanatory
        overwrite : True/False, optional
            Self explanatory. The default is False.

        """

        # PLace first HDU as only header, descripting the file
        # PLace 2nd HDU as cluster data
        # PLace 3rd HDU as spectral data
        hdr = fits.Header()
        hdr["DISTANCE"] = self.distance
        hdr["DISTANCE_UNIT"] = "pc"
        hdr["EXT1"] = "Cluster data"
        hdr["EXT2"] = "Isochrone data"
        hdr["EXT3"] = "Spectra data"
        empty = fits.PrimaryHDU(header=hdr)
        
        
        hdu_data = fits.BinTableHDU(self.data)
        hdu_iso = fits.BinTableHDU(self.isochrone)
        hdu_spectral = fits.BinTableHDU(self.spectraldata)
        
        hdul = fits.HDUList([empty, hdu_data, hdu_iso, hdu_spectral])
        hdul.writeto(filename, overwrite = overwrite)
    
    
    def apply_extinction(self, filter = "K", extinction = None):
        """
        Applies extinction in the given 'filter', either depending on the position or a constant value.

        Parameters
        ----------
        filter : str, optional
            Filter to add the extinction to. The default is "K".
        extinction : None or float, optional
            None : use the 'EXTINCTION_FUNCTION' parameter to compute the extinction.
            float : constant value for the extinction, in terms of magnitude.

        Returns
        -------
        None.
        Adds the new weight in a new self.data column with name 'weight_ext_FILTERNAME'

        """
        print("Applying extinction for filter {}".format(filter))
        if extinction is not None:
            ext = extinction
        else:
            ext = self.cmds["EXTINCTION_FUNCTION"](cluster_data = self.data, target_filter = filter)
        weights = self.data["weight"]
        new_weights = 10**(-0.4 * (np.add(-2.5*np.log10(weights), ext)))
        column_name = "weight_ext_" + filter
        
        self.data[column_name] = new_weights
    
    
    
    def compute_magnitude(self, filter = "Ks", use_weight = None, use_filter = "default", zp = "default"):
        """
        Computes the apparent magnitude of each star in a new self.data column of name 'magFILTERNAME'
        Be careful, this might take a long time if spectra are high resolution, as they are by default.

        Parameters
        ----------
        filter : str, optional
            filter name in which to compute the magnitude. The default is "Ks".
        use_weight : str, optional
            weight column to use for the computation. If None (default), will compute the magnitude with the 'weight' column (no extinction).
        use_filter : 'default' or length = 2 list/tuple, optional
            If 'default', will use the basic Bessel or 2MASS filters
            Else, must contain the shape of the filter. use_filter[0] are the wavelengths, use_filter[1] are the values.
        zp : 'default' or float, optional
            'default' : uses regular Vega zeropoints for the filters
            if a value is given, it must be in 'erg/s/cm2/A'.
            
        """
        
        lams = self.spectraldata["Lambda (Angstrom)"]
        spectra = self.spectraldata["Flux (erg/cm2/s/A)"]
        # print(np.shape(spectra))
        # print(np.shape(lams))
        if use_weight is not None:
            weights = self.data[use_weight]
        else:
            weights = self.data["weight"]
        
        refs = self.data["ref"]
        
        mags = ut.compute_magnitude(lams = lams, spectra = spectra, filter = filter, weights = weights, refs = refs, use_filter = use_filter, zp = zp)
        
        self.data["mag{}".format(filter)] = mags


        
def load_default_parameters():
    """
    Loads the default parameters from the cmds module
    """
    print("Loading default parameters")
    return cmds.cmds()






def make_simcado_source(clobj = None,
                        filename = None,
                        weight_name = None,
                        rebining = "default",
                        savename = "source.fits",
                        min_wavelength = 0.1,
                        max_wavelength = 2.5,
                        bins_wavelength = 5000,
                        resample_type = "linear"):
    """
    Makes a SimCADO source file from a Cluster object

    Parameters
    ----------
    clobj : cluster object, optional
        cluster object to turn into a SimCADO Source File
    filename : str, optional
        if the cluster object was saved to a file, use this parameter.
    weight_name : str, optional
        colmumn name in of the clusterobject.data to use as weight. By default, will use the 'weight' column. This is useful if you wihs to use a specific weight for extinction.
    rebining : str or list, optional
        Default : 'default', will use following parameters to for the spectra resampling.
        list : wavelengths bins center to use for the Source object
    savename : str, optional
        save name for the SimCADO Source object. The default is "source.fits".
    min_wavelength : float, optional
        if rebining is 'default, use this a the minimum wavelength value (in um). Default : 0.1 um.
    max_wavelength : float, optional
        if rebining is 'default, use this a the maximum wavelength value (in um). Default : 2.5 um
    bins_wavelength : int, optional
        if rebining is 'default, number of wavelength bins. The default is 5000.
    resample_type : 'str', optional
        'linear' or 'log'. Resampling type for the bins spacing. KEEP THIS TO 'LINEAR' AS 'LOG' CAN LEAD TO WEIRD THINGS IN SIMCADO. The default is "linear".
        
    Returns
    -------
    src : SimCADO Source objet

    """
    
    if weight_name is None:
        weight_name = "weight"
        print("""
              --------------------------- Warning ---------------------------
              No 'weight_name' was given, the 'weight' column will be used
              ---------------------------------------------------------------
              """)
    
    import simcado as sim
    
    if filename is not None:
        clobj = cluster(filename = filename)
    data = clobj.data
    xproj = data["xproj_[arcsec]"].data
    yproj = data["yproj_[arcsec]"].data
    refs = data["ref"].data
    weight = data[weight_name].data
    distance = clobj.distance
    
    
    lam = clobj.spectraldata["Lambda (Angstrom)"]
    spectra = clobj.spectraldata["Flux (erg/cm2/s/A)"]
    
    # Rebining the spectra ?
    # if given a list, it must be the wavelengths in Angstrom
    lam_min = None
    if rebining != "default":
        new_lam = rebining
    else:
        # searching for the lowest non zero wavelength
        for thelams in lam:
            for i in range(len(thelams)):
                if thelams[i] != 0:
                    to_check = thelams[i]
                    break
                else:
                    continue
            
            if lam_min is None:
                lam_min = to_check
            elif to_check < lam_min:
                lam_min = to_check
        # searching for the highest wavelength
        lam_max = None
        for thelams in lam:
            to_check = thelams[-1]
            if lam_max is None:
                lam_max = thelams[-1]
            elif to_check > lam_max:
                lam_max = to_check
                
        # making new wevelength bins between these lam_min and lam_max
        if resample_type == "log":
            new_lam = np.logspace(np.log10(max(lam_min, min_wavelength*10000)), np.log10(min(lam_max, max_wavelength*10000)), bins_wavelength)
        elif resample_type == "linear":
            new_lam = np.linspace(max(lam_min, min_wavelength*10000), min(lam_max, max_wavelength*10000), bins_wavelength)
        else:
            raise ValueError("'resample_type' must be either 'log' or 'linear'")
    
    
    
    print("Resampling spectra between {:.3f} um and {:.3f} um".format(np.min(new_lam/10000), np.max(new_lam/10000)))
    
    dlam = (new_lam[1:] - new_lam[:-1])
    dlam = np.append(dlam,dlam[-1]) # bin size, used later when scaling the spectra in ph/s/m2
    dlam_aa = dlam
    dlam_um = ((dlam_aa*u.AA).to(u.um)).value
    
    # evaluating the spectra in these new wavelengths
    # print("Rebining spectra")
    rebined_spectra = []
    for spec,this_lam in zip(spectra,lam):
        # The spectra are in unit of flux density erg/cm2/s/A
        # Evaluating the spectra at new wavelenghts bins
        spec_fun = interp1d(this_lam, spec, bounds_error = False, fill_value = (0, 0))
        new_spec = spec_fun(new_lam)
        
        rebined_spectra.append(new_spec)
                
    
    # Converting spectra to erg/cm2/s/um
    rebined_spectra = [((rebined_spectra[i] * (1/u.AA)).to(1/u.um)).value for i in range(len(rebined_spectra))]
    
    # Converting lams to um
    new_lam = ((new_lam * u.AA).to(u.um)).value
    
    # Rescaling the spectra to ph/s/m2
    for i in range(len(rebined_spectra)):
        this_spectra = rebined_spectra[i]
        
        # Converting /cm2 to /m2
        this_spectra = np.dot(this_spectra, 1e4)
        
        # Converting erg to Joules
        this_spectra = (this_spectra * u.erg).to(u.joule).value
        
        # E = h*nu = h*c/lam, energy per photon, so dividing each value per E returns ph/s/m2/um
        E = const.h.value * const.c.value / (((new_lam*u.um).to(u.m)).value)
        this_spectra = np.multiply(1/E, this_spectra)
        
        # Spectra is in ph/s/m2/um
        # Now multiplying by bin size
        this_spectra = np.multiply(this_spectra, dlam_um)
        
                
        rebined_spectra[i] = this_spectra

    src = sim.source.Source(lam = np.asarray(new_lam),
                            spectra = np.asarray(rebined_spectra),
                            x = xproj,
                            y = yproj,
                            ref = refs,
                            weight = weight,
                            units="ph/s/m2")
        
    src.info["distance"] = distance*u.pc
    
    src.write(savename)
    return src



















