


import numpy as np
from os.path import join
from . import utils as ut
stdatadir = join(ut.__pkg_dir__, "data")




class imf:
    """
    IMF class that is called in the cluster object 'generate' method
    """
    def __init__(self, cmds = None, **kwargs):
        if cmds is None:
            from .cluster import load_default_parameters
            cmds = load_default_parameters()
        for key in kwargs:
            if key in cmds:
                cmds[key] = kwargs[key]
        self.cmds = cmds
    
    def generate(self, number = None, total_mass = None, **kwargs):
        print("Generating stars masses")
        masses = self.cmds["IMF"].random(number = number, total_mass = total_mass, **kwargs)
        return masses
        




class imf_base:
    """
    IMF base class
    __________
    Each IMF subclass must contain the methods :
        - norm : normalizes the IMF distribution 
        - evaluate : returns the value of the IMF distribution for te specified mass
        - inv_imf : generate a list of random masses according to the considered IMF
    Contains subclasses for different types of IMF :
        - Salpeter
        - Log-Normal (TODO)
        - Segmented power law
    """
    def __init__(self):
        self.normalize()
    
    def normalize(self):
        self.norm()
    
    def random(self, number = None, total_mass = None, verbose = False):
        """
        Generates star masses until number or total_mass is reached.
        """
        if total_mass is None:
            if verbose == True:
                print("Both 'number' and 'total_mass' are None, therefore it will generate a single star as if 'number = 1'")
            if number == 1:
                number = None
            c = np.random.uniform(size = number)
            masses = self.inv(c)
            return masses
        else:
            sum = 0
            masses = []
            while sum < total_mass:
                val = self.random(number = None)
                masses.append(val)
                sum += val
                ut.printProgressBar(sum, total_mass, length = 50)
            print("Asked mass = {} M_sol".format(total_mass))
            print("Final mass = {} M_sol".format(np.sum(masses)))
            print("Generated a total of {} stars".format(len(masses)))
            return masses
            
    
    def getminmass(self):
        return self.mass_ranges[0]
    
    def getmaxmass(self):
        return self.mass_ranges[-1]



class seg_powerlaw(imf_base):
    """
    IMF sub-class for segmented-power laws IMFs, such as Kroupa+2001
    """
    def __init__(self, mass_ranges, power_ranges): #,  hdr = HDR.copy(), name = "Segmented Power Law"):
        # hdr["NAME"] = name
        # hdr["ANALYTICAL_FORM"] = "dN/dM = K_i * M^-alpha_i on each segment [m_i : m_i+1]"
        
        if not np.all(np.diff(mass_ranges) >0):
            raise ValueError("masse_ranges should be ordered by increasing mass")
        if len(mass_ranges) -1 != len(power_ranges):
            raise ValueError("mass_ranges should be of length 1+len(power_ranges)")
        
        for i in range(len(power_ranges)):
            self.mass_ranges = mass_ranges
            self.power_ranges = power_ranges
            
        self.nseg = len(power_ranges)
        super().__init__()
        
    def norm(self, **kwargs):
        """
        Normalizes the distribution
        """
        masses, powers = self.mass_ranges, self.power_ranges
        nseg = self.nseg
        sum = 0
        for i in range(nseg):
            prod = 1
            for k in range(1,i+1):
                prod *= masses[k]**(powers[k]-powers[k-1])
            powe = 1-powers[i]
            sum += prod * (masses[i+1]**(powe) - masses[i]**(powe))/(powe)
        self.normfact = sum
        
        
        self.normfactinteg = self.evaluate_dlogm(1, normalize = True)
        
        # Integral values for successive segments
        integ = []
        tot = 0
        for i in range(nseg):
            val = 1
            for k in range(1,i+1):
                val *= masses[k]**(powers[k] - powers[k-1])
            val /= self.normfact
            val *= (masses[i+1]**(1-powers[i]) - masses[i]**(1-powers[i]))/(1-powers[i])
            integ.append(val + tot)
            tot = integ[-1]
        self.integvals = integ
    
    
    def evaluate(self, mass):
        """
        Returns the value of the distribution for a given mass
        """
        masses, powers = self.mass_ranges, self.power_ranges
        index = 0
        while True:
            if masses[index] <= mass and masses[index+1]>= mass:
                break
            index += 1
        
        prod = 1
        for k in range(1,index+1):
            prod *= masses[k]**(powers[k] - powers[k-1])
        prefact = prod/self.normfact
        out = prefact * mass**(-powers[index])
        return out
    
    
    
    def evaluate_dlogm(self, mass, normalize = False, avoid_norm = False):
        """
        Returns the value of the distribution for a given mass, on a dN/dlogM scale
        """
        masses, powers = self.mass_ranges, self.power_ranges
        
        if normalize == True:
            m = np.logspace(np.log10(masses[0]), np.log10(masses[-1]), 2000)
            m[0] = masses[0]
            m[-1] = masses[-1]
            vals = [self.evaluate_dlogm(mass = m[i], normalize = False, avoid_norm = True) for i in range(len(m))]
            integ = np.trapz(vals, np.log10(m))
            return integ
        
        
        index = 0
        while True:
            if masses[index] <= mass and masses[index+1]>= mass:
                break
            index += 1
        
        if avoid_norm is False:
            normfact = self.normfactinteg
        else:
            normfact = 1
        
        prod = 1
        for k in range(1,index+1):
            prod *= masses[k]**(powers[k] - powers[k-1])
        prefact = prod/normfact
        out = prefact * mass**(-powers[index] +1)
        return out
        
    
    def evaluate_integ(self, mass):
        """
        Evaluates the integral value in a certain mass
        """
        masses, powers = self.mass_ranges, self.power_ranges
        index = 0
        while True:
            if masses[index] <= mass and masses[index+1]>= mass:
                break
            index += 1
        if index == 0:
                base = 0
        else:
            base = self.integvals[index -1]
        prod = 1
        for k in range(1,index+1):
            prod *= masses[k]**(powers[k] - powers[k-1])
        prefact = prod/self.normfact
        out = base + prefact/(1-powers[index]) * (mass**(1-powers[index])-masses[index]**(1-powers[index]))
        return out
    
    def inv(self, clist):
        """
        Check in which segment to search for the mass
        then can be called in the 'random' method of the imf_base class
        """
        
        masses, powers = self.mass_ranges, self.power_ranges
        # Check wether it was given a list or a unique number of c's
        try:
            iter(clist)
            testiter = True
        except:
            testiter = False
        if testiter == False:
            clist = [clist]
        vallist = []
        
        # Iterates on the list of c's
        count = 0
        for c in clist:
            if testiter == True:
                ut.printProgressBar(count, len(clist) -1)
            index = 0
            while self.integvals[index] < c:
                index += 1
            if index == 0:
                base = 0
            else:
                base = self.integvals[index -1]
            val = 1
            for k in range(1,index+1):
                val *= masses[k]**(powers[k] - powers[k-1])
            val = ((c-base)*(1-powers[index]) * (self.normfact / val) + masses[index]**(1-powers[index]))**(1/(1-powers[index]))
            vallist.append(val)
            count +=1
        
        # Formats the output to the input format : number or list
        if len(vallist) == 1:
            val = vallist[0]
        else:
            val = vallist
        return val

class kroupa(seg_powerlaw):
    """
    Kroupa IMF, as given in equation (2) in Kroupa.2000: On the variation of the initial mass function.
    """
    def __init__(self):
        mass_ranges = [0.01, 0.08, 0.50, 1.0, 1000]
        powers = [0.3, 1.3, 2.3, 2.3]
        seg_powerlaw.__init__(self,mass_ranges= mass_ranges, power_ranges = powers) #, name = "Kroupa IMF -- Segmented Power Law")

class lognormal(imf_base):
    """
    #TODO Log Normal, Chabrier 2003 2005
    
    """
    def __init__(self, M_min, M_max): #, hdr = HDR.copy()):
        #hdr["NAME"] = "Log Normal"
        #hdr["ANALYTICAL_FORM"] = "dN/dM = M^-alpha"
        #hdr["alpha"] = 2.35
        self.M_min = M_min
        self.M_max = M_max
        super().__init__()
        
    def norm(self, **kwargs):
        #TODO
        return 0
    
    def evaluate(self, mass):
        #TODO
        return 1
    
    def inv(self, c):
        #TODO
        return 1


class powerlaw(seg_powerlaw):
    """
    Power law IMF object
    ---------
    Follow a distribution : dN/dM = K * M^-alpha
    """
    
    def __init__(self, M_min, M_max, alpha = 2.35):
        """
        Default is alpha = -2.35, which is the historical Salpeter value.
        the subclass salpeter also exists and does the same thing.
        """
        power_ranges =  [alpha]
        mass_ranges = [M_min, M_max]

        super().__init__(mass_ranges, power_ranges)
    

    def norm(self, **kwargs):
        """
        Normalizes the distribution to 1 between M_min and M_max
        Also updates M_min and M_max values if given
        """
        keys = {"alpha":None,
                "M_min":None,
                "M_max":None}
        for key in keys:
            if key in kwargs:
                keys[key] = kwargs[key]
            elif key in self.hdr:
                keys[key] = self.hdr[key]
            else:
                raise ValueError("Missing keyword '{}' in **kwargs or hdr. Either give it or initiate the imf object with a predefined one".format(key))
        alpha = self.power_ranges[0]
        M_min = self.mass_ranges[0]
        M_max = self.mass_ranges[-1]
        
        K = (1-alpha)/(M_max**(1-alpha) - M_min**(1-alpha))
        self.normfact = K
    
    def evaluate(self, mass):
        """
        Returns the value of the distribution for a given mass.
        Since the ditribution is normalized to 1, it is required to multiply the output by the total mass of the studied star population.
        NOTE : If the output is negative, it is likely that the distribution has not yet been normalized.
        """
        K = self.normfact
        alpha = self.power_ranges[0]
        number = K * mass**(-alpha)
        return number
    
    def inv(self, c):
        """
        Returns the value of 'mass' for which the integral from M_min to 'mass' is equal to the random value c given in input.
        -> Basically returns a random mass within the current imf probability distribution.
        """
        
        alpha = self.power_ranges[0]
        M_min = self.mass_ranges[0]
        M_max = self.mass_ranges[-1]
        
        power = 1-alpha
        mass = (c*(M_max**power - M_min**power) + M_min**power)**(1/power)
        
        return mass

class salpeter(powerlaw):
    def __init__(self, M_min, M_max):
        super().__init__(self, M_min = M_min, M_max = M_max, alpha = 2.35)#, name = "Salpeter")
