#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 11:36:13 2020

@author: demarsd
"""



import numpy as np
from scipy.interpolate import interp1d, interp2d
from . import utils as ut

from . import __data_dir__ as stdir
from astropy.table import Table
from astropy.io import fits
from astropy.io import ascii





class spectral:
    """
    Called from clustermodel.cluster.cluster
    """
    
    
    def __init__(self, data, cmds = None, **kwargs):
        if cmds is not None:
            self.cmds = cmds
        else:
            self.cmds = cmds
        self.cluster_data = [data, self.cmds]
        if "distance" in kwargs:
            self.distance = kwargs["distance"]
    
    def run(self):
        """
        Reads spectra
        Loads isochrone
        Associate each star to a spectra
        Weights each star's spectra ccording to the isochrone'
        """
        
        self.read_spectra()         # Spectra in self.spectradata as a FITS_REC array
        self.load_isochrone()       # Isochrone into self.isochrone as an astropy table
        self.associate_spectra()    # Adds 'ref' column to clusterobject data
        self.weight_spectra()       # Adds 'weight' column to clusterobject data
        
        weights = self.weights
        refs = self.ref
        
        self.cluster_data[0]["weight"] = weights
        self.cluster_data[0]["ref"] = refs
        
        return self.cluster_data[0], self.spectraldata, self.isochrone
    
    def read_spectra(self):
        """
        Reads the spectra
        """
        self.spectraldata = self.cmds["READ_SPECTRA"](self.cmds["SPECTRA_FILENAME"])
    
    
    def associate_spectra(self):
        """
        Associates each star to a spectrum
        """
        print("Associating spectra")
        # self.cmds["ASSOCIATE_SPECTRA"] takes as an argument the data from the cluster, as well as the output of self.cmds["READ_SPECTRA"], and self.cmds["READ_ISOCHRONE"]
        ref = self.cmds["ASSOCIATE_SPECTRA"](self.cluster_data, self.spectraldata, self.isochrone)
        self.ref = ref
        return 0
    
    def load_isochrone(self):
        """
        Loads the isochrone
        """
        print("Loading Isochrone")
        isochrone = self.cmds["READ_ISOCHRONE"](**self.cmds["READ_ISOCHRONE_ARGS"])
        self.isochrone = isochrone
    
    def weight_spectra(self):
        """
        Weighs the spectra for each star. No extinction there, as this will be computed with extinction later in the cluster obejct 'generate' sequence
        """
        print("Adding Weight")
        # self.cmds["WEIGHT_SPECTRA"] takes two arguments : the cluster table itself, and the isochrone as the output of self.cmds["READ_ISOCHRONE"], and the cmds if required for a specific way to handle the extinction
        self.weights = self.cmds["WEIGHT_SPECTRA"](self.cluster_data, self.isochrone, distance = self.distance, spec_norm = self.cmds["SPECTRA_NORMALIZATION"])


def weight_spectra(cluster_data, isochrone, distance, extinction = None, spec_norm = "K"):
    """
    Weighs the spectra of each star.
    returns the list of weights

    Parameters
    ----------
    cluster_data : cluster data Table from cluster.data.
    isochrone : isochrone Table from spectra.isochrone
    distance : Distance from the cluster
    extinction : None or float, optional
        None : use the 'EXTINCTION_FUNCTION' parameter to compute the extinction.
        float : constant value for the extinction, in terms of magnitude.
    spec_norm : str, optional
        Filter to which the spectra are normalized . The default is "K".

    Returns
    -------
    weights : list

    """
    
    masses_ini = cluster_data[0]["Mass_ini"]
    fun_Mv = interp1d(np.log10(isochrone["Mass_ini"]), isochrone[spec_norm], bounds_error = False, fill_value = (isochrone[spec_norm][0], isochrone[spec_norm][-1])) # Function that associates a mass to a Mv
    
    weights = []
    for i in range(len(masses_ini)):
        Mv = fun_Mv(np.log10(masses_ini[i]))
        ext = 0
        DM = 5*np.log10(distance) - 5
        
        if extinction is not None and extinction is not False:
            if extinction == True:
                ext = cluster_data[1]["EXTINCTION_FUNCTION"](cluster_data = cluster_data[0], cmds = cluster_data[1])
            else:
                ext = extinction
        
        weights += [10**(-0.4*(Mv + ext + DM))]
    return weights


def add_extinction(cluster_data = None,
                   cmds = None,
                   xpos = None,
                   ypos = None,
                   target_filter = None,
                   each_coord = True,
                   get_ratio_K = False,
                   extinction_map = stdir +"/arches_Ks_extinction.fits",
                   map_filter = "K"):
    """
    Returns the extinction in a given filter 

    Parameters
    ----------
    cluster_data : cluster object data, optional
        if given, will return the extinction for all stars in the cluster, depending on their position xproj/yproj. The default is None.
    cmds : clustermodel.cmds object, optional
    
    xpos : list, optional
        list of x projected position in arcsec. The default is None.
     xpos : list, optional
        list of y projected position in arcsec. The default is None.
    target_filter : str, optional
        Target filter for the extinction. The default is None.
    each_coord : True/False, optional
        If True, will return the extinction in each couple of postion. If False, will return the extinction for each possible couple of position.
        Example :
            False : xpos = [1,2,3], ypos = [5,6], returns : extinctions for postions (1,5) (1,6) (2,5) (2,6) (3,5) (3,6)
    get_ratio_K : True/False, optional
        If True, will return the ratios of the Rieke law instead.
    extinction_map : str, optional
        Path to the extinction map. The default is stdir +"/arches_Ks_extinction.fits".
    map_filter : filter in which the extinction map is, optional
        DESCRIPTION. The default is "K".

    Returns
    -------
    list
        List of extinctions.

    """
    # Loading extinction map
    if cluster_data is not None:
        xpos = cluster_data["xproj_[arcsec]"]
        ypos = cluster_data["yproj_[arcsec]"]
        
    # If given cmds, will its parameters instead of the function defaults
    if cmds is not None:
        if target_filter is None:
            target_filter = cmds["TARGET_FILTER"]
        extinction_map = cmds["EXTINCTION_MAP"]
        map_filter = cmds["EXTINCTION_MAP_FILTER"]
    
    # Loading extinction map
    ext = fits.open(extinction_map)
    ext_val = ext[1].data
    ext_coords = ext[2].data
    x = [ext_coords[0][i][0] for i in range(len(ext_coords[0]))]
    y = [ext_coords[i][0][1] for i in range(len(ext_coords))]
    
    # Loading Rieke law
    rieke_law = ascii.read(stdir + "/" + "rieke_extinction_law.dat")
    fr = rieke_law["Filter_name"]
    ar = rieke_law["A_lambda/Av"]
    rieke = {fr[i]:ar[i] for i in range(len(fr))}
    
    if get_ratio_K == True:
        print("K:", rieke["K"] / rieke["K"])
        print("H:", rieke["H"] / rieke["K"])
        print("J:", rieke["J"] / rieke["K"])
        print("I:", rieke["I"] / rieke["K"])
        return 0
    
    # Computing factor by which to rescale the extinction
    # The Arches map extinction map is in units of A_Ks
    # Thus we multiply the whole map by (A_filter/A_V) * (A_V/A_Ks), ie : rieke["filter"] / rieke["Ks"]
    ext_factor = rieke[target_filter] / rieke[map_filter]
    ext_val = np.dot(ext_val, ext_factor)
    
    # Building the list of extinction for each star, depending on its position
    ext_fun = interp2d(x,y,ext_val, bounds_error = False, fill_value = 0)
    
    # Convenient checks in case it was given a single coord not encapsulated in a list.
    if hasattr(xpos, "__iter__") and hasattr(ypos, "__iter__"):
        if each_coord == True:
            extinctions = []
            for x,y in zip(xpos,ypos):
                ext = ext_fun(x,y)
                extinctions.append(ext[0])
        else:
            extinctions = ext_fun(xpos,ypos)
    else:
        extinctions = ext_fun(xpos,ypos)[0]
    
    return extinctions



def associate_spectra(cluster_data ,spectra_data, isochrone):
    """
    Associates each star to a spectrum

    Parameters
    ----------
    cluster_data : cluster object data
        
    spectra_data : clustermodel.spectral.spectral.spectraldata
        contains Teff, Lambda and values of each spectra
    isochrone : clustermodel.spectral.spectral.isochrone
        Isochrone as defined in the cmds object
    Returns
    -------
    list
        list of reference to spectra for each star

    """
    # Get the list of stars identified by their mass
    masses = cluster_data[0]["Mass_[Msun]"]
    teff = spectra_data["Teff (K)"]
    fun_teff = interp1d(isochrone["Mass_ini"], isochrone["Teff"], bounds_error = False, fill_value = (teff[0], teff[-1])) # function that associates a mass to a Teff
    
    ref = []
    for i in range(len(masses)):
        star_teff = fun_teff(masses[i])
        idx, val = ut.find_nearest(teff, star_teff) #finds the nearest teff in the spectra teff
        ref += [idx]
        # print(ref)
    
    return ref # the list of references to the spectra
    


def read_spectra(filename):
    """
    Reads spectra
    """
    content = fits.open(filename)[1].data
    return content




def load_isochrone(directory = stdir + "/isochrones", mass_lim = 0.8):
    """
    Loads the isochrone
    """
    
    btfilenames = ["btsettl_bessel.dat", "btsettl_2mass.dat"]
    parsfilenames = ["parsec_bessel.dat", "parsec_2mass.dat"]
    btisochrone = Table()
    for name in btfilenames:
        table_data = Table.read(directory + "/" + name, format='ascii.basic')
        for col in table_data.colnames:
            if col not in btisochrone.colnames:
                btisochrone[col] = table_data[col]
    
    parsisochrone = Table()
    for name in parsfilenames:
        table_data = Table.read(directory + "/" + name, format='ascii.basic')
        for col in table_data.colnames:
            if col not in parsisochrone.colnames:
                parsisochrone[col] = table_data[col]
    
    # print(parsisochrone.colnames)
    # Building the total isochrone
    isochrone = Table()
    cols = ["Mass_ini", "Mass", "Teff", # common parameters
            "U", "B", "V", "R", "I", # Bessel filters magniudes
            "J", "H", "Ks"] # 2MASS filters magnitudes
    
    # Function to build formated columns in the isochrone
    def build_column(colname, btis = btisochrone, parsis = parsisochrone, mass_lim = mass_lim):
        mbt = btis["M/Ms"]
        mpars = parsis["Mini"]
        
        # Checking idx of limits between isochrone
        for i in range(len(mbt)):
            if mbt[i] > mass_lim:
                idxlim_bt = i - 1
                break
        for i in range(len(mpars)):
            if mpars[i] > mass_lim:
                idxlim_pars = i
                break
        
        # Getting data from each isochrone
        btdata = {"Mass_ini": btis["M/Ms"][0:idxlim_bt+1],
                  "Mass": btis["M/Ms"][0:idxlim_bt+1],
                  "Teff": btis["Teff(K)"][0:idxlim_bt+1],
                  "U": btis["Ux"][0:idxlim_bt+1],
                  "B": btis["B"][0:idxlim_bt+1],
                  "V": btis["V"][0:idxlim_bt+1],
                  "R": btis["R"][0:idxlim_bt+1],
                  "I": btis["I"][0:idxlim_bt+1],
                  "J": btis["J"][0:idxlim_bt+1],
                  "H": btis["H"][0:idxlim_bt+1],
                  "Ks": btis["Ks"][0:idxlim_bt+1]}
        
        parsdata = {"Mass_ini": parsis["Mini"][idxlim_pars:],
                  "Mass": parsis["Mass"][idxlim_pars:],
                  "Teff": 10**parsis["logTe"][idxlim_pars:],
                  "U": parsis["UXmag"][idxlim_pars:],
                  "B": parsis["Bmag"][idxlim_pars:],
                  "V": parsis["Vmag"][idxlim_pars:],
                  "R": parsis["Rmag"][idxlim_pars:],
                  "I": parsis["Imag"][idxlim_pars:],
                  "J": parsis["Jmag"][idxlim_pars:],
                  "H": parsis["Hmag"][idxlim_pars:],
                  "Ks": parsis["Ksmag"][idxlim_pars:]}
        

        # Appending both parts of the isochrone
        final_column_data = np.append(btdata[col], parsdata[col])
        
        return final_column_data
    
    for col in cols:
        data = build_column(col)
        isochrone[col] = data
    
    return isochrone
    
    
    



def load_isochrone_old(directory = stdir + "/isochrones",
                   filename = ["BTSettl_VEGA_t0.003.txt", "parsec.txt"],
                   filter = "V",
                   mass_lim = 0.8):
    """
    Deprecated
    """
    if filter == "Ks":
        filter = "K"
    # Get list of magnitude, depending on mass / effective temperature
    m_bt, T_effbt, V_bt = get_btsettl_mag(directory = directory, filename = filename[0], filter = filter)
    m_pars, T_effpars, V_pars = get_parsec_mag(directory = directory, filename = filename[1], filter = filter)
    
    #Search for the idx of the mass that is nearest to 50Msol
    for i in range(len(m_pars)):
        if m_pars[i] > 50 or T_effpars[i] > 70000:
            break
    idx = i
    # print(idx)
    # idx = 176
    m_pars  = m_pars[:idx+1]
    T_effpars = T_effpars[:idx+1]
    V_pars = V_pars[:idx+1]
    
    # We need to define a limit mass or effective temperature from which we use Parsec instead of BTSettl isochrones
    # We define it as when Mv(Parsec) > Mv(BTSettl)
    # We use the transition mass : m = 0.8 Msol
    # We thus buid the lists mass, T_eff, V_list
    mass = []
    T_eff = []
    V_list = []
    for i in range(len(m_bt)):
        if m_bt[i] > mass_lim:
            break
        else:
            mass.append(m_bt[i])
            T_eff.append(T_effbt[i])
            V_list.append(V_bt[i])
    for i in range(len(m_pars)):
        if m_pars[i]<mass_lim:
            continue
        else:
            mass.append(m_pars[i])
            T_eff.append(T_effpars[i])
            V_list.append(V_pars[i])
    
    return [mass, T_eff, V_list]

def get_btsettl_mag(directory = stdir + "/isochrones",
                    filename = None,
                    filter = "V"):
    """
    Used in the old version of load_isochrone
    """

    if filename is None:
        if filter in ["J,H,Ks"]:
            filename = "btsettl_2mass.dat"
        else:
            filename = "btsettl_bessel.dat"
    btsettlfilename = directory + "/" + filename
    table_data = Table.read(btsettlfilename, format='ascii.basic')
    m = table_data["M/Ms"]
    teff= table_data["Teff(K)"]
    v = table_data[filter]
    return m, teff, v

def get_parsec_mag(directory = stdir + "/isochrones",
                   filename = None,
                   filter = "V"):
    """
    Used in the old version of load_isochrone
    """
    if filename is None:
        if filter in ["J,H,Ks"]:
            filename = "parsec_2mass.dat"
        else:
            filename = "parsec_bessel.dat"
    btsettlfilename = directory + "/" + filename
    table_data = Table.read(btsettlfilename, format='ascii.basic')
    m = table_data["Mini"]
    teff= 10**(table_data["logTe"])
    v = table_data["{}mag".format(filter)]
    return m, teff, v



    














