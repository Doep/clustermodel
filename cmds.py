
import dill

from . import imf
from . import binary
from . import position
from . import spectral

from . import __data_dir__ as stdir


class cmds(dict):
    def __init__(self, filename = None):
        """
        cmds module/class. Use the 'save' method to write the cmds to a file, use the 'filename' parameter to read the cmds from a file
        Example :
            param = cmds()
            cmds.save("my_commands.obj")
            reload_param = cmds("my_commands.obj")
            
        """
        
        super().__init__()
        
        if filename is None:
            self.load_default_parameters()
        else:
            self.load(filename)
    
    def load_default_parameters(self):
        dic = {}
        dic["IMF_CLASS"] = imf.imf                      # Non initialized IMF base class, has the 'generate' method that defines how to use the dic["IMF"]
        dic["IMF"] = imf.kroupa()                       # Initialized IMF class
        
        
        
        dic["BINARY_CLASS"] = binary.binary                   # Set to False instead for no binaries. Non initialized Binary base class, has the 'generate' method that manages the use of binary related entries in the dictionnary.
        dic["MRATIO_SEP_MODEL"] = binary.model                # Non initialized class that manages BASE_MRATIOs and BASE_SEP models
        dic["BASE_MRATIO"] = binary.model_mass_ratio()        # Initialized class used in 'MRATIO_SEP_MODEL', this should not be changed
        dic["BASE_SEP"] = binary.model_separation()           # Initialized class used in 'MRATIO_SEP_MODEL', this should not be changed
        dic["MULTIPLICITY"] = binary.model_multiplicity()     # Initialized class containing the multiplicity model
        
        
        dic["POSITION_CLASS"] = position.position                                           # Non initialized class that manages the use of 'SPATIAL_DISTRIBUTION', 'MASS_SEGREGATION' and 'COORDINATES_MAKING' functions.
        dic["SPATIAL_DISTRIBUTION"] = position.EFF(**{"a":0.13, "gamma":2.3, "rmax":3})     # Initialized class for the spatial distribtution. Change parameters according to the cluster
        dic["MASS_SEGREGATION"] = position.mass_segregation(s = 0.3)                        # Initialized class for the mass segregation. Change parameters according to the cluster
        dic["COORDINATES_MAKING"] = position.make_coordinates()                             # Initialized class that makes the coordinates of the systems. This should probably not be changed.
        
        
        dic["SPECTRAL_CLASS"] = spectral.spectral                                                       # Non initialized spectral base class
        dic["READ_SPECTRA"] = spectral.read_spectra                                                     # Function to read spectra
        dic["SPECTRA_FILENAME"] = stdir + "/spectra/" + "spectra_Ks.fits.gz"   # Arguments to pass to the 'READ_SPECTRA' function. Spectra must be normalized to mag = 0, with ag the filter defined in 'SPECTRA_NORMALIZATION'
        dic["SPECTRA_NORMALIZATION"] = "Ks"                                                              # Filter to which the spectra are normalized
        dic["READ_ISOCHRONE"] = spectral.load_isochrone                                                 # Function to read the isochrone
        dic["READ_ISOCHRONE_ARGS"] = {"directory": stdir + "/isochrones"}                               # Arguments to pass to the 'READ_ISOCHRONE' function
        dic["ASSOCIATE_SPECTRA"] = spectral.associate_spectra                                           # Function that associates a star to a spectra
        dic["WEIGHT_SPECTRA"] = spectral.weight_spectra                                                 # Function that weights the spectra for each star
        
        dic["EXTINCTION"] = True                                                # True/False, add extinction to the weight
        dic["EXTINCTION_FUNCTION"] = spectral.add_extinction                    # Function that adds extinction to each star
        dic["TARGET_FILTER"] = "K"                                              # Filter in which to add the extinction
        dic["EXTINCTION_MAP"] = stdir +"/arches_Ks_extinction.fits"             # Extinction map to use in 'EXTINCTION_FUNCTION'
        dic["EXTINCTION_MAP_FILTER"] = "K"                                      # Filter in which the extinction map gives the extinction
        
        
        dic["BREAK_HIGH_SEPARATIONS"] = True                                      # True/False Compute the mean density in the cluster core, and break binaries that have separation higher than twice the mean distance between stars in the core
        dic["AGE_CLUSTER"] = True                                               # Compute the mass of star now versus at their birth, according to the isochrone
        
        
        for key in dic.keys():
            self[key] = dic[key]
        
        
    def save(self, savename):
        # Save the cmds to a file
        # dill.dump(self, savename)
        with open(savename, "wb") as dill_file:
            dill.dump(self, dill_file)
    
    def load(self, filename):
        # Load the cmds from a file
        with open(filename, "rb") as f:
            f = dill.load(f)
        for key in f.keys():
            self[key] = f[key]
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            