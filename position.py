#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 11:22:46 2020

@author: demarsd
"""

import numpy as np
from scipy.interpolate import interp1d
from os.path import join
from . import utils as ut
stdatadir = join(ut.__pkg_dir__, "data")
import astropy .units as u
from astropy.table import Table






class position:
    """
    Position class, called in the cluster object 'generate' method
    """
    def __init__(self, cmds = None, **kwargs):
        if cmds is None:
            from .cluster import load_default_parameters
            cmds = load_default_parameters()
        for key in kwargs:
            if key in cmds:
                cmds[key] = kwargs[key]
        self.cmds = cmds
    
    
    def generate(self, sys, **kwargs):
        """
        Compute 3D coordinates of the input systems.
        Binary systems must have already been made and given in 'sys' parameter
        """
        systems = sys[0]
        aged_systems = sys[1]
        
        # Breaking high separation systems
        if self.cmds["BREAK_HIGH_SEPARATIONS"] == True:
            sys = self.cmds["SPATIAL_DISTRIBUTION"].break_systems(systems, aged_systems)
            systems = sys[0]
            aged_systems = sys[1]
        
        print("Computing distance to center of each system")
        # Calls the spatial dsitribution, default is Elson, Fall & Freeman 1987 density profile
        radius = self.cmds["SPATIAL_DISTRIBUTION"].random(systems = systems)
        
        #If segregation parameter is not None, mass-segregate the cluster
        if self.cmds["MASS_SEGREGATION"] is not None:
            print("Mass segregating the cluster")
            systems, radius = self.cmds["MASS_SEGREGATION"].segregate(sys, radius)
        
        # Getting output cluster as an astropy Table
        thecluster = self.cmds["COORDINATES_MAKING"].random(sys, radius)
        
        return thecluster




class make_coordinates:
    """
    Distance to center of each system have already been made and are given with 'radius' parameter in the 'random' method.
    Places the star in the x,y,z coordinates of the cluster
    """
    def __init__(self):
        self.a = 0
    
    def random(self, sys, radius):
        # systems = sys[0]
        # aged_systems = sys[1]
        
        print("Computing xyz coordinates of each system")
        pos = make_systems_pos(radius)
        
        print("Computing xyz coordinates of each star")
        masses, pos, multiplicity, linked_to = make_stars_pos(sys, pos)
        
        thecluster = Table([masses, pos[0], pos[1], pos[2], multiplicity, linked_to], names = ("Mass_ini", "x_[pc]", "y_[pc]", "z_[pc]", "Multiplicity", "Linked_to"))
        
        return thecluster
    

def make_systems_pos(radius):
    """
    Places each system in the cluster
    """
    if not hasattr(radius,"__iter__"):
        radius = [radius]
    x, y, z = [], [], []
    for r in radius:
        theta = np.random.uniform() * 2*np.pi
        phi = np.random.uniform() * 2*np.pi
        x.append(r * np.sin(theta) * np.cos(phi))
        y.append(r * np.sin(theta) * np.sin(phi))
        z.append(r * np.cos(theta))
    if np.nan in x:
        print("In x")
    if np.nan in y:
        print("In y")
    if np.nan in z:
        print("In z")
    if len(x) == 1:
        return x[0],y[0],z[0]
    else:
        return x,y,z

def make_stars_pos(sys, pos):
    """
    Make systems position depending on multiplicity and separation
    Places stars around the system's barycentre depending on the mass ratio'
    """
    systems_ini = sys[0]
    systems = sys[1]
    masses = []
    oldx, oldy, oldz = pos[0], pos[1], pos[2]
    x,y,z = [],[],[]
    multiplicity = []
    linked_to = []
    
    for i in range(len(systems)):
        system = systems[i]
        sys_ini = systems_ini[i]
        
        # If system is made of a single star
        if system[-1] is None:
            masses.append(sys_ini[0])
            x.append(oldx[i])
            y.append(oldy[i])
            z.append(oldz[i])
            multiplicity.append(1)
            linked_to.append(len(linked_to))
        
        # If system is made of two stars
        else:
            mass_1 = system[0]
            mass_ini_1 = sys_ini[0]
            mass_2 = system[1]
            mass_ini_2 = sys_ini[1]
            q = mass_2 / mass_1         # mass ratio computed as the ratio of current masses, not inital masses
            
            # Checking that mass ratio are all below 1
            if q > 1:
                raise ValueError("q should be lower than 1 {}, {}".format(mass_2, mass_1))
            
            s = ((system[-1] * u.au).to(u.pc)).value
            r1 = q * s/(1+q)
            r2 = s/(1+q)
            
            # Random theta and phi, spherical symmetry
            theta = np.random.uniform() * 2*np.pi
            phi = np.random.uniform() * 2*np.pi
            
            
            dx1 = r1 * np.sin(theta) * np.cos(phi)
            dy1 = r1 * np.sin(theta) * np.sin(phi)
            dz1 = r1 * np.cos(theta)
            dx2 = r2 * np.sin(theta) * np.cos(phi)
            dy2 = r2 * np.sin(theta) * np.sin(phi)
            dz2 = r2 * np.cos(theta)
            x.append(oldx[i] + dx1)
            x.append(oldx[i] - dx2)
            y.append(oldy[i] + dy1)
            y.append(oldy[i] - dy2)
            z.append(oldz[i] + dz1)
            z.append(oldz[i] - dz2)
            masses.append(mass_ini_1)
            masses.append(mass_ini_2)
            multiplicity.append(2)
            multiplicity.append(2)
            linked_to.append(len(linked_to)+1)
            linked_to.append(len(linked_to)-1)
    newpos = [x,y,z]
    return masses,newpos, multiplicity, linked_to



class mass_segregation:
    """
    The class that handles the mass segregation. It uses the MCluster method
    """
    def __init__(self, s):
        self.s = s
    
    def segregate(self, sys, radius):
        """
        Make a mass-segregated list of radius as a function of mass, in a similar way as in MCluster
        Check my intership report for details
        """
        systems = sys[0]
        aged_systems = sys[1]
        
        # Sorting radius
        radius = np.sort(radius)
        # Sorting systems by decreasing total mass
        totmass = []
        for i in range(len(systems)):
            if systems[i][-1] is None:
                totmass.append(aged_systems[i][0])
            else:
                lenmax = len(systems[i])
                totmass.append(sum([aged_systems[i][k] for k in range(lenmax -1)]))
        sorted_systems_idx = np.argsort(totmass)[::-1]
        
        out_radius = []
        out_systems = []
        N = len(sorted_systems_idx)
        for i in range(N):
            X = np.random.uniform()
            idx = int((N - i) * (1 - X**(1- self.s)))  # Randomizing the radius-index of current mass
            count = 0
            for j in range(len(radius)):    # i-th system takes the place of the idx-th first available radius
                if count == idx:
                    out_radius.append(radius[j])
                    out_systems.append(systems[sorted_systems_idx[i]])
                    radius = np.delete(radius, j)
                    ut.printProgressBar(i,N-1)
                    break
                else:
                    count +=1
        if np.nan in out_radius:
            raise ValueError("This shouldn't enter this IF")
        return out_systems, out_radius






class EFF:
    """
    Position distribution along Elson, Fall & Freeman (1987) distribution
    rho(R) = rho_0 * (1 + (R/a)^2)^(-(gamma+1)/2)
    """
    def __init__(self, a, gamma, rmax, rmin = 0):
        self.a = a
        self.gamma= gamma
        self.rmin = rmin
        self.rmax = rmax
        self.norm = 1
        self.make_distrib()
        rc = self.a * (2**(2/self.gamma) -1)**0.5   # core radius
        self.rc = rc
    
    def norm(self):
        return 0
    
    def evaluate(self, R):
        """
        evaluate de distribution in a given radius
        """
        gamma = self.gamma
        a = self.a
        val = (1/self.norm) * (1 + (R/a)**2)**(-(gamma+1)/2)
        return val
        
    def make_distrib(self, length = 2001):
        x = np.linspace(self.rmin,self.rmax, length, endpoint = True)
        y = [self.evaluate(x[i]) for i in range(len(x))]
        integ = [np.trapz(y[0:k+1], x[0:k+1]) for k in range(len(y))]
        norm = integ[-1]
        integ = np.dot(integ, 1/norm)
        self.norm = norm
        fun = interp1d(integ, x)
        self.function = fun
        fun_integ = interp1d(x, integ)
        self.function_integ = fun_integ
    
    def random(self, number = None, systems = None):
        """
        return a random radius according to the EFF distribution, as many times as asked with the 'number' parameter. If None, will return one value.
        You can also give the binary systems themselves, it will check for how many sytsems there are.
        """
        if systems is not None:
            number = len(systems)
        
        elif number == 1:
            number = None
        
        c = np.random.uniform(size = number)
        val = self.function(c)
        return val
    
    def break_systems(self, systems, systems_aged):
        """
        If you want to break systems taht have a too high separation.
        This simulates the effect of systems that have crossed the dense core of the cluster.
        """
        print("Systems breaking is ON")
        # Computes the mean density in the core radius of the cluster
        # r_c = a*(2**(2/gamma) -1)**(1/2)
        # Computes the mean separation between systems in that radius
        # Break systems that have a separation higher than this value 
        
        # Compute the mean density in the core
        prop = self.function_integ(self.rc) # prop: proportion of stars within the core radius
        nb_center = prop * len(systems)
        volume = 4/3 * np.pi * self.rc**3
        dens = nb_center/volume
        
        # Compute the mean separation between two systems in the cluster core
        sep_mean = (((1/dens)**(1/3) * u.pc).to(u.au)).value
        print("Separation limit : {:.2f} AU".format(2*sep_mean))
        
        # Now, we break systems that have a separation higher than twice the mean distance between two systems 'sep_mean'
        end_systems = []
        end_systems_aged = []
        for i in range(len(systems)):
            if systems[i][-1] is None:
                sys = [systems[i]]
                sys_aged  = [systems_aged[i]]
            else:
                sep = systems[i][-1]
                if sep > 2* sep_mean:
                    # breaking the system
                    sys = [[systems[i][0], None], [systems[i][1], None]]
                    sys_aged = [[systems_aged[i][0], None], [systems_aged[i][1], None]]
                else:
                    sys = [systems[i]]
                    sys_aged = [systems_aged[i]]
            
            end_systems += sys
            end_systems_aged += sys_aged
        
        return end_systems, end_systems_aged
        
        
        
        
        
        
        
        
        
        
        
