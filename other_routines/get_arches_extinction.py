# -*- coding: utf-8 -*-
"""
Created on Sat May 16 13:50:37 2020

@author: Dorian
"""



from astropy.io import fits
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from PIL import Image
from stagem2.utils import stdir
import matplotlib.pyplot as plt


# Change to your own path
os.chdir(stdir)

# Change to the name of the image
ext_img = "arches_Ks_extinction.png"
fits_savename = "arches_Ks_extinction.fits"
img_savename = "arches_recovered_Ks_extinction.png"

##### Image parameters
# Image limits
im_upleft = (136,34)        # Graph limits coordinates in pixel
im_botright = (702,568)     # Graph limits coordinates in pixel

# x axis
im_x_pos = (134,654)        # x axis limits coordinates in pixel
im_x_values = (-60,60)      # x axis limits, physical values
xlog = False                # True/False for log scale

# im_x_y_pos = 572



# y axis
im_y_pos = (50,572)         # y axis limits coordinates in pixel
im_y_values = (60,-60)      # y axis limits, physical values
ylog = False                # True/False for log scale
# im_y_x_pos = 134




##### Color bar parameters
# Position parameters
cb_xcoord = 750     # x coordinates for the colorbar #WARNING : Please choose a value in the middle of the colorbar so it avoids any black tick
cb_ymin = 28        # (ymin) coordinate of the colorbar in pixel
cb_ymax = 576       # (ymax) coordinate of the colorbar in pixel


# Value parameters
cb_log_scale = False        
cb_valmax_pos = 539         # Position of the maximum value on the colorbar in pixels
cb_values = (3.00,1.80)     # Values on the colorbar
cb_val_pos = (65,539)       # Correpsonding position on the colorbar (y pixels values)

cb_valmax_pos = cb_val_pos[1]
cb_valmin_pos = cb_val_pos[0]



def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 50, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd)
    # Print New Line on Complete
    if iteration >= total: 
        print()


def get_physical_coordinates(pixelpos,
                             im_x_values = im_x_values,
                             im_y_values = im_y_values):
    
    xpos = pixelpos[0]
    ypos = pixelpos[1]
    if xpos < im_upleft[0] or xpos > im_botright[0] or ypos < im_upleft[1] or ypos > im_botright[1]:
        raise ValueError("This pixel  : {} is not within the image".format(pixelpos))
    
    
    if xlog == True:
        im_x_values = np.log10(im_x_values)
        dx = (xpos - im_x_pos[0]) / (im_x_pos[1] - im_x_pos[0])
        xphys = dx * (im_x_values[1] - im_x_values[0])
        xphys = 10**xphys
    else:
        dx = (xpos - im_x_pos[0]) / (im_x_pos[1] - im_x_pos[0])
        xphys = dx * (im_x_values[1] - im_x_values[0])
    
    if ylog == True:
        im_y_values = np.log10(im_y_values)
        dy = (ypos - im_y_pos[0]) / (im_y_pos[1] - im_y_pos[0])
        yphys = dy * (im_y_values[1] - im_y_values[0])
        yphys = 10**yphys
    else:
        dy = (ypos - im_y_pos[0]) / (im_y_pos[1] - im_y_pos[0])
        yphys = dy * (im_y_values[1] - im_y_values[0])
    
    xphys += im_x_values[0]
    yphys += im_y_values[0]
    
    return (xphys, yphys)



def make_colorbar(imdata):

    cb_valmin = cb_values[0]
    cb_valmax = cb_values[1]
    
    
    colorbar_colors = []
    colorbar_values = []
    for y in range(cb_ymin, cb_ymax):
        colorbar_colors.append(imdata[cb_xcoord, y])
        
        dy = (y - cb_valmin_pos) / (cb_valmax_pos - cb_valmin_pos)
        if cb_log_scale == True:
            cb_valmax = np.log10(cb_valmax)
            cb_valmin = np.log10(cb_valmin)
        
        val = dy * (cb_valmax - cb_valmin)
        
        if cb_log_scale == True:
            val = 10**val
        
        val += cb_valmin
        colorbar_values.append(val)
    
    return colorbar_colors, colorbar_values
        
    




im = Image.open(ext_img, "r")

imdata = im.load()
xsize, ysize = im.size

cb_colors, cb_val = make_colorbar(imdata)



# We will browse the image
# For each pixel, compute the distance with each pixel of the colorbar



out_datas = []
out_coords = []



for y in range(im_upleft[1], im_botright[1] +1):
    printProgressBar(y-im_upleft[1], im_botright[1]-im_upleft[1] +1)
    line_data = []
    line_coords = []
    for x in range(im_upleft[0], im_botright[0] +1):
        pixel = imdata[x,y]
        
        
        # Computing distances between pixel value and colormap color list
        distances = []
        for i in range(len(cb_colors)):
            color = cb_colors[i]
            
            # Computing squared difference over RGB colors
            dist = 0
            for c1,c2 in zip(pixel, color):
                dist += (c2-c1)**2
            distances.append(np.sqrt(dist))
        
        # Finding the minimum value
        idxmin = np.array(distances).argmin()

        line_data.append(cb_val[idxmin])
        line_coords.append(get_physical_coordinates((x,y)))
    out_datas.append(line_data)
    out_coords.append(line_coords)


print("")
print("Saving to fits file : {}".format(fits_savename))


hdu1 = fits.ImageHDU(out_datas)
hdu2 = fits.ImageHDU(out_coords)

hdr = fits.Header()
hdr["EXT1"] = "Ks data value"
hdr["EXT2"] = "coords in arcsec (x,y)"
hdu0 = fits.PrimaryHDU(header = hdr)

hdul = fits.HDUList([hdu0, hdu1, hdu2])

hdul.writeto(fits_savename)
# 






# Plotting gathered data

x = [out_coords[0][i][0] for i in range(len(out_coords[0]))]
y = [out_coords[i][0][1] for i in range(len(out_coords))]


plt.figure(figsize = (16,12))
plt.pcolormesh(x,y,out_datas, cmap = "jet")
plt.xlabel("ΔX (arcsec)")
plt.ylabel("ΔY (arcsec)")
plt.title("A_Ks (mag) extinction map")
plt.colorbar()
plt.legend()
plt.savefig(img_savename)






















