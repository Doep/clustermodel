# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 15:06:33 2020

@author: Dorian
"""
import stagem2.utils as ut
from scipy.interpolate import interp1d
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import os
import simcado as sim
import numpy as np
import astropy.units as u


filters = ["H", "Ks"]
stoutdir = "D:/User Documents/Documents/Cours/M2 - Astro/Stage/Simulations/models/Arches/IMF-Kroupa/data_starfinder/"

#%%

for filt in filters:
    psf = fits.open(sim.__data_dir__ + "/PSF_MAORY_{}.fits".format(filt))
    psf_data = psf[0].data
    norm = np.sum(psf_data)
    pd = np.divide(psf_data,norm)
    pixscale = ((psf[0].header["CDELT1"] * u.deg).to(u.arcsec)).value *1000
    lenpsf = len(psf_data)*pixscale
    coordsmids = [i*pixscale - (lenpsf - lenpsf/2) for i in range(len(psf_data))]
    thesum = 0
    for i in range(len(psf_data)):
        for j in range(len(psf_data)):
            if (coordsmids[i] < -512*1.5 or coordsmids[i] > 512*1.5) and (coordsmids[j] < -512*1.5 or coordsmids[j] > 512*1.5):
                thesum += pd[i][j]
    print(filt, thesum)
    
    
    
#%%

for filt in filters:
    psf = fits.open(sim.__data_dir__ + "/PSF_MAORY_{}.fits".format(filt))
    psf_data = psf[0].data
    pixscale = ((psf[0].header["CDELT1"] * u.deg).to(u.arcsec)).value *1000
    lenpsf = len(psf_data)*pixscale
    coordsmids = [i*pixscale - (lenpsf - lenpsf/2) for i in range(len(psf_data))]
    # idx, a = ut.find_nearest(coordsmids, -512*1.5)
    # gpsf = psf_data[idx:len(psf_data)-idx,idx:len(psf_data)-idx]
    gpsf = psf_data
    gpsf = np.divide(gpsf, np.sum(gpsf))
    
    outpsf = fits.open(stoutdir + "PSF_{}.fits".format(filt))
    outpsf_data = outpsf[0].data
    outpsf_data = np.dot(outpsf_data, (np.max(gpsf)/np.max(outpsf_data)) * (1.5/pixscale)**2)
    
    print(filt, np.sum(gpsf)/np.sum(outpsf_data))
    
    
    
    
    
    
    
    