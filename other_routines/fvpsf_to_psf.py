# -*- coding: utf-8 -*-
"""
Created on Sat May 30 12:04:25 2020

@author: Dorian
"""


from astropy.io import fits
import simcado as sim
import os
import numpy as np

os.chdir(sim.__data_dir__)
filt = ["H", "I", "J", "Ks"]
psfs = ["PSF_FV_MAORY_H.fits", "PSF_FV_MAORY_I.fits", "PSF_FV_MAORY_J.fits", "PSF_FV_MAORY_Ks.fits"]

for name,f in zip(psfs,filt):
    psf = fits.open(name)
    print(name)
    print(np.shape(psf[2].data[0]))
    hdu = fits.PrimaryHDU(psf[2].data[0], header = psf[2].header)
    hdul = fits.HDUList([hdu])
    hdul.writeto("PSF_MAORY_{}.fits".format(f), overwrite = True)