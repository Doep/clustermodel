#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 14:50:11 2020

@author: demarsd
"""

import clustermodel.utils as ut
import numpy as np
import os
import astropy
from astropy.io import fits
from astropy.modeling.physical_models import BlackBody
from astropy import units as u
from clustermodel import __data_dir__ as stdir

directory = "D:/User Documents/Documents/Cours/M2 - Astro/Stage bis/raw spectra/"

target_filter = "Ks"
V_aim = 0



os.chdir(directory)




filenames = []

for f in os.listdir(directory):
    if os.path.isfile(os.path.join(directory, f)) and f.endswith(".dat.txt.gz"):
            filenames.append(f)


lams = []
specs = []
teff = []
for file in filenames:
    print("----------------------------")
    print("Reading spectra : '{}'".format(file))
    if file[6] == ".":
        Teff = float(file[3:8])*100
    else:
        Teff = float(file[3:6])*100
    teff.append(Teff)
    spectra = astropy.io.ascii.read(file, format='no_header')
    spectra_lam = spectra["col1"].data
    spectra_val = spectra["col2"].data
    
    lams.append(spectra_lam)
    specs.append(spectra_val)








# Building BlackBody spectra above 70000K

teff_to_build = [80000, 90000, 100000,
                 110000, 120000, 130000, 140000, 150000,
                 160000, 170000, 180000, 190000, 200000,
                 210000, 220000, 230000, 240000, 250000]

bb_lams= []
for i in range(len(lams[-1])):
    if lams[-1][i] !=0:
        bb_lams.append(lams[-1][i])


for bb_teff in teff_to_build:
    bb = BlackBody(temperature = bb_teff *u.K, scale = 1.0* u.erg / (u.AA * u.s * u.sr * u.cm**2))
    spectra_val = bb(bb_lams* u.AA).value
    teff.append(bb_teff)
    lams.append(bb_lams)
    specs.append(spectra_val)








# Computing current magnitudes
V_cur = ut.compute_magnitude(lams, specs, filter = target_filter)


factor =  10**(-0.4 * np.subtract(V_aim, V_cur))


specs_rescaled = [np.dot(factor[i], specs[i]) for i in range(len(specs))]







# Building big file with spectra
# Building columns 
col1 = fits.Column(name = "Teff (K)", format = "K", array = teff)

col2 = fits.Column(name = "Lambda (Angstrom)", format = "PE()",
                   array = np.array(lams, dtype = np.object_))

col3 = fits.Column(name = "Flux (erg/cm2/s/A)", format = "PE()",
                   array = np.array(specs_rescaled, dtype = np.object_))

# Building the hdu

hdu = fits.BinTableHDU.from_columns([col1, col2, col3])
hdu.header["SCALING"] = "{} = {}".format(target_filter, V_aim)


hdu.writeto(stdir + "/spectra/" + 'spectra_Ks.fits.gz')








