# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 11:21:54 2020

@author: Dorian
"""


from clustermodel import __data_dir__ as stdir
import numpy as np
import matplotlib.pyplot as plt
import clustermodel.utils as ut
from astropy.io import fits
from astropy.table import Table
import astropy.units as u
import astropy
import os
import simcado
from os import listdir
from os.path import isfile, join
from scipy.interpolate import interp1d
from astropy.io import ascii
import gzip


# table_data = Table.read(filename, format='ascii')


def get_btsettl_V_mag(filename = "BTSettl_VEGA_t0.003.txt"):
    isodir =stdir + "\isochrones"
    btsettlfilename = os.path.join(isodir ,filename)
    table_data = Table.read(btsettlfilename, format='ascii.basic')
    m = table_data["M/Ms"]
    teff= table_data["Teff(K)"]
    v = table_data["V"]
    return m, teff, v

def get_parsec_V_mag(filename = "parsec.txt"):
    isodir =stdir + "\isochrones"
    btsettlfilename = os.path.join(isodir ,filename)
    table_data = Table.read(btsettlfilename, format='ascii.basic')
    m = table_data["Mass"]
    teff= 10**(table_data["logTe"])
    v = table_data["Vmag"]
    return m, teff, v


def get_param_lists_separation_btsettl_parsec(mass_lim = 0.8):
    # Get list of magnitude, depending on mass / effective temperature
    m_bt, T_effbt, V_bt = get_btsettl_V_mag()
    m_pars, T_effpars, V_pars = get_parsec_V_mag()
    
    # We only use parsec isochrone up to Mass=21.695, after which Teff starts going down
    # This corresponds to idx = 176, Teff = 35083.26, Mass = 21.695
    # After this, we will use SimCADO's way of building spectra
    idx = 176
    m_pars  = m_pars[:idx+1]
    T_effpars = T_effpars[:idx+1]
    V_pars = V_pars[:idx+1]
    
    # We need to define a limit mass or effective temperature from which we use Parsec instead of BTSettl isochrones
    # We define it as when Mv(Parsec) > Mv(BTSettl)
    # We use the transition mass : m = 0.8 Msol
    # We thus buid the lists mass, T_eff, V_list
    mass = []
    T_eff = []
    V_list = []
    for i in range(len(m_bt)):
        if m_bt[i] > 0.8:
            break
        else:
            mass.append(m_bt[i])
            T_eff.append(T_effbt[i])
            V_list.append(V_bt[i])
    for i in range(len(m_pars)):
        if m_pars[i]<0.8:
            continue
        else:
            mass.append(m_pars[i])
            T_eff.append(T_effpars[i])
            V_list.append(V_pars[i])
    
    return mass, T_eff, V_list

def normalize_spectra(write_files = True):
    # Zero-point for V filter
    q_zeropoint = 3.619e-9 #erg/s/cm^2/angstrom
    
    
    # Get V filter curve
    V_filter = ascii.read(stdir + "/filters" + "/Bessell_V.dat")
    V_lam = V_filter["col1"].data
    V_val = V_filter["col2"].data
    
    lam_min = np.min(V_lam)
    lam_max = np.max(V_lam)
    
    # V Filter function
    filter_fun = interp1d(V_lam, V_val)
    
    
    spectra_path = stdir + '/spectra/'
    spectranames = []
    for f in listdir(spectra_path):
        if isfile(join(spectra_path, f)) and f.endswith(".dat.txt"):
            spectranames.append(f)

    os.chdir(spectra_path)
    
    
    # Integrating zeropoint flux over the filter
    zeropoint_flux_per_lambda = np.dot(V_val, q_zeropoint)
    
    #
    #
    #
    # Multiplying by lambda
    zeropoint_flux_per_lambda = np.multiply(zeropoint_flux_per_lambda, V_lam)
    # if timeslam == True:
    #     zeropoint_flux_per_lambda = np.multiply(zeropoint_flux_per_lambda, V_lam)
    #
    #
    #
    #
    
    flux_ref = np.trapz(zeropoint_flux_per_lambda, V_lam)
    # print(flux_ref)
    
    # Computing lower and upper limits in Teff.
    # teffmin = np.min(T_eff)
    # teffmax = np.max(T_eff)
    # plt.plot(T_eff)
    # plt.plot(np.linspace(3000,34000,1000), mag_fun(np.linspace(3000,34000,1000)))
    # plt.show()
    # return 0
    
    for filename in spectranames:
        print("----------------------------")
        print("Normalizing spectra : '{}'".format(filename))
        #Effective temperature of current spectra
        Teff = int(filename[3:6]) *100
        # if Teff < teffmin:
        #     print("Current spectra could not be normalized : Teff is below minimum value in the isochrone")
        #     continue
        # elif Teff > teffmax:
        #     print("Current spectra could not be normalized : Teff is above maximum value in the isochrone")
        #     continue
        
        # Getting desired V mag, computed by interpolation of T_eff over V_list
        V_aim = 0
        print("Required V magnitude : {}".format(V_aim))
        
        # Getting values of spectra
        spectra = astropy.io.ascii.read(filename, format='no_header')
        # spectra_lam = (spectra["col1"]*u.angstrom).to(u.um).value
        # spectra_val = spectra["col2"] / ((u.angstrom).to(u.um))
        spectra_lam = spectra["col1"].data
        spectra_val = spectra["col2"].data
        
        # Convolve spectra by this filter
        if lam_min < np.min(spectra_lam):
            lam_min = np.min(spectra_lam)
        if lam_max > np.max(spectra_lam):
            lam_max = np.max(spectra_lam)
        
        # filtered_vals contains V-filtered spectrum
        filtered_vals = []
        for i in range(len(spectra_lam)):
            lam = spectra_lam[i]
            if lam < lam_min:
                filtered_vals.append(0)
            elif lam > lam_max:
                filtered_vals.append(0)
            else:
                filtered_vals.append(spectra_val[i] * filter_fun(lam))
        
        # Integrating spectra
        
        
        filtered_vals = np.multiply(filtered_vals, spectra_lam)
        # print("1", np.min(filtered_vals), np.max(filtered_vals))
        
        integrated_flux = np.trapz(filtered_vals, spectra_lam)
        
        # Computing current V magnitude of the spectra
        V_cur = -2.5 * np.log10(integrated_flux / flux_ref)
        # print(integrated_flux)
        print("Current V magnitude : {}".format(V_cur))
        
        # Computing rescaling factor : K = 10**(-(V_aim - V_cur)/2.5)
        K =  10**(-(V_aim - V_cur)/2.5)
        print("Flux rescaling factor : K = {}".format(K))
        rescaled_flux = np.dot(K, integrated_flux)
        rescaled_spectra = np.dot(K, spectra_val)
        
        # Checking Magnitude after rescaling:
        rescaled_mag = -2.5 * np.log10(rescaled_flux / flux_ref)
        print("Magnitude after rescaling flux : {}".format(rescaled_mag))
        
        # Saving rescaled spectra to a fits file
        str_teff = str(Teff).zfill(6)
        savename = "btsettl_teff_" + str_teff + "_V0.fits"

        
        if write_files == True:
            print("Saving to : '{}'".format(savename))
            spectra_table = Table([spectra_lam, rescaled_spectra], names = ("lambda (Angstrom)", "Flux (erg/cm2/s/A)"))
            spectra_table.write(savename, format = "fits")
    
    return 0



# mbt, teffbt, vbt = get_btsettl_V_mag()
# mpars, teffpars, vpars = get_parsec_V_mag()

# plt.plot(mbt, vbt, label = "btsettl")
# plt.plot(mpars, vpars, label = "parsec")
# plt.xlabel("Mass")
# plt.ylabel("Magnitude")
# plt.xscale("log")
# plt.xlim((min(mpars), max(mbt)))
# plt.legend()
# plt.show()

# plt.plot(teffbt, vbt, label = "btsettl")
# plt.plot(teffpars, vpars, label = "parsec")
# plt.xlabel("Teff")
# plt.ylabel("Magnitude")
# plt.xscale("log")
# plt.legend()
# plt.show()

# plt.plot(mbt,teffbt, label = "btsettl")
# plt.plot(mpars, teffpars, label = "parsec")
# plt.xlabel("Mass")
# plt.ylabel("Teff")
# plt.yscale("log")
# plt.legend()
# plt.show()

# for i in range(len(teffpars)):
#     cur = teffpars[i]
#     fol = teffpars[i+1]
#     if mpars[i]>20:
#         val = i
#         break
# print("""Starts going down after,
#       idx = {},
#       Teff = {}, 
#       Mass = {}""".format(val, teffpars[val], mpars[val]))


