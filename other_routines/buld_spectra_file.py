#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 16:26:08 2020

@author: demarsd
"""

from astropy.io import fits
import numpy as np
from astropy.table import Table
from astropy import units as u
import os
from clustermodel import __data_dir__ as stdir
from astropy.modeling.physical_models import BlackBody
from scipy.interpolate import interp1d
from astropy.io import ascii

teffs = []
lams = []
specs = []
os.chdir(stdir + "/spectra")

for file in sorted(os.listdir()):
    if file.endswith(".fits"):
        teff = int(file[13:19])
        # print(teff)
        # if teff[-1] == "_":
        #     teff = int(teff[:-1])
        # else:
        #     teff = int(teff)
        teffs.append(teff)
        data = fits.open(file)[1].data
        lam = data["lambda (Angstrom)"]
        # print(lam)
        spectra = data["Flux (erg/cm2/s/A)"]
        lams.append(lam)
        specs.append(spectra)


# print(teffs)
#%%

# Building BlackBody spectra above 70000K

teff_to_build = [80000, 90000, 100000,
                 110000, 120000, 130000, 140000, 150000,
                 160000, 170000, 180000, 190000, 200000,
                 210000, 220000, 230000, 240000, 250000]


spectra_lam = lams[-1]
# print(type(spectra_lam))
# print(np.shape(spectra_lam))
for bb_teff in teff_to_build:
    bb = BlackBody(temperature = bb_teff *u.K, scale = 1.0* u.erg / (u.AA * u.s * u.sr * u.cm**2))
    spectra_val = bb(spectra_lam* u.AA).value
    # scale the BB to V = 0
    
    q_zeropoint = 3.619e-9
    V_filter = ascii.read(stdir + "/filters" + "/Bessell_V.dat")
    V_lam = V_filter["col1"].data
    V_val = V_filter["col2"].data
    lam_min = np.min(V_lam)
    lam_max = np.max(V_lam)
    # V Filter function
    filter_fun = interp1d(V_lam, V_val)
    spectra_path = stdir + '/spectra/'
    spectranames = []
    
    # Integrating zeropoint flux over the filter
    zeropoint_flux_per_lambda = np.dot(V_val, q_zeropoint)
    
    # Multiplying by lambda
    zeropoint_flux_per_lambda = np.multiply(zeropoint_flux_per_lambda, V_lam)
    
    flux_ref = np.trapz(zeropoint_flux_per_lambda, V_lam)
    
    V_aim = 0
    if lam_min < np.min(spectra_lam):
        lam_min = np.min(spectra_lam)
    if lam_max > np.max(spectra_lam):
        lam_max = np.max(spectra_lam)
    
    # filtered_vals contains V-filtered spectrum
    filtered_vals = []
    for i in range(len(spectra_lam)):
        lam = spectra_lam[i]
        if lam < lam_min:
            filtered_vals.append(0)
        elif lam > lam_max:
            filtered_vals.append(0)
        else:
            filtered_vals.append(spectra_val[i] * filter_fun(lam))
    
    # Integrating spectra
    
    
    filtered_vals = np.multiply(filtered_vals, spectra_lam)
    # print("1", np.min(filtered_vals), np.max(filtered_vals))
    
    integrated_flux = np.trapz(filtered_vals, spectra_lam)
    
    # Computing current V magnitude of the spectra
    V_cur = -2.5 * np.log10(integrated_flux / flux_ref)
    # print(integrated_flux)
    print("Current V magnitude : {}".format(V_cur))
    
    # Computing rescaling factor : K = 10**(-(V_aim - V_cur)/2.5)
    K =  10**(-(V_aim - V_cur)/2.5)
    print("Flux rescaling factor : K = {}".format(K))
    rescaled_flux = np.dot(K, integrated_flux)
    rescaled_spectra = np.dot(K, spectra_val)
    
    # Checking Magnitude after rescaling:
    rescaled_mag = -2.5 * np.log10(rescaled_flux / flux_ref)
    print("Magnitude after rescaling flux : {}".format(rescaled_mag))
    
    teffs.append(bb_teff)
    lams.append(spectra_lam)
    specs.append(rescaled_spectra)

#%%



# Building big file with spectra
# Building columns 
col1 = fits.Column(name = "Teff (K)", format = "K", array = teffs)

col2 = fits.Column(name = "Lambda (Angstrom)", format = "PE()",
                   array = np.array(lams, dtype = np.object_))

col3 = fits.Column(name = "Flux (erg/cm2/s/A)", format = "PE()",
                   array = np.array(specs, dtype = np.object_))

# Building the hdu

hdu = fits.BinTableHDU.from_columns([col1, col2, col3])

hdu.writeto('spectra.fits.gz')