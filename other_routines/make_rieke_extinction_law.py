# -*- coding: utf-8 -*-
"""
Created on Sat May 16 20:58:38 2020

@author: Dorian
"""

from astropy.io import fits
from astropy.io import ascii
import numpy as np
import os
from stagem2.utils import stdir
from astropy.table import Table

os.chdir(stdir)

A_lam_over_A_V = {"U": 1.531,
                  "B": 1.324,
                  "V": 1.000,
                  "R": 0.748,
                  "I": 0.482,
                  "J": 0.282,
                  "H": 0.175,
                  "Ks": 0.112,
                  "L": 0.058,
                  "M": 0.023,
                  "N": 0.052}

names = [name for name in A_lam_over_A_V]
values = [A_lam_over_A_V[name] for name in A_lam_over_A_V]


cl = Table([names,values,], names = ("Filter_name", "A_lambda/Av"))

ascii.write(cl, 'rieke_extinction_law.dat')