


"""
Use this to divide 12kx12k simcado images into 9 4kx4k smaller pieces
"""

import stagem2.utils as ut
from astropy.io import fits
import os
import numpy as np



directory = "D:/User Documents/Documents/Cours/M2 - Astro/Stage/Simulations/models/Arches/IMF-Kroupa/"

filenames = ["simcado_image_I.fits", "simcado_image_J.fits","simcado_image_Ks.fits"]


os.chdir(directory)



"""


The image will be divided this way:

    im0 | im1 | im2
    ----------------
    im3 | im4 | im5
    ----------------
    im6 | im7 | im8

Note this representation assumes that the pixel of coordinates (0,0) is the top left corner.
This is not correct in DS9 for example, where the (0,0) pixel corresponds to the bottom left corner.
In such a representation, this would look like : 
    im6 | im7 | im8
    ----------------
    im3 | im4 | im5
    ----------------
    im0 | im1 | im2

"""


for filename in filenames:
    fitsfile = fits.open(filename)
    im = fitsfile[0].data
    hdr = fitsfile[0].header
    
    images = []
    hdrs = []
    for row in range(3):
        for col in range(3):
            tmp_im = im[row*4096:(row+1)*4096, col*4096:(col+1)*4096]
            tmp_hdr = hdr.copy()
            tmp_hdr["NAXIS1"] = 4096
            tmp_hdr["NAXIS2"] = 4096
            tmp_hdr["CRPIX1"] = tmp_hdr["CRPIX1"] - row*4096
            tmp_hdr["CRPIX2"] = tmp_hdr["CRPIX2"] - col*4096
            tmp_hdr["CRPIX1A"] = tmp_hdr["CRPIX1A"] - row*4096
            tmp_hdr["CRPIX2A"] = tmp_hdr["CRPIX2A"] - col*4096
            print("Shape of row-{} col-{} : ".format(row,col), np.shape(tmp_im))
            images.append(tmp_im)
            hdrs.append(tmp_hdr)
    
    # Saving images
    noextname, ext = os.path.splitext(filename)
    
    for i in range(len(images)):
        img = images[i]
        head = hdrs[i]
        hdu = fits.PrimaryHDU(img, header = head)
        hdul = fits.HDUList([hdu])
        savename = noextname + "_im{}".format(i) + ext
        print("Saving to : {}".format(savename))
        hdul.writeto(savename, overwrite = True)
    
